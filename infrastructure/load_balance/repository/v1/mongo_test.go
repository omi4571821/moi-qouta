package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	mockMongo "moi-quota/mock/driver/mongo"
	"moi-quota/pkg/api/infrastructure/v1/load_balance"
	"reflect"
	"testing"
)

func Test_mongoContext_CreateQuotaLoadBalance(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success insert
	mockCollectionInsertSuccess := mockMongo.NewMockCollection(ctrl)
	mockSingleResult := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	loadBalanceCountSuccess := 10
	mockCollectionInsertSuccess.EXPECT().InsertOne(ctxSuccess, &LoadBalanceQuota{
		Quota: int64(loadBalanceCountSuccess),
	}).Return(nil, nil)
	instanceSuccess := &LoadBalanceQuota{}
	gomock.InOrder(
		mockCollectionInsertSuccess.EXPECT().FindOne(gomock.Any(), gomock.Any()).Return(mockSingleResult),
		mockSingleResult.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(instanceSuccess))
			return nil
		}),
	)
	inSuccess := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: int64(loadBalanceCountSuccess),
	}
	// success update
	mockCollectionUpdateSuccess := mockMongo.NewMockCollection(ctrl)
	mockSingleResultUpdate := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxUpdateSuccess := context.Background()
	loadBalanceCountUpdateSuccess := 10
	idUpdateSuccess := primitive.NewObjectID()
	resultUpdateSuccess := pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
		UpsertedCount: 0,
	}
	mockCollectionUpdateSuccess.EXPECT().UpdateOne(ctxUpdateSuccess, gomock.Any(), gomock.Any()).Return(&resultUpdateSuccess, nil)
	gomock.InOrder(
		mockCollectionUpdateSuccess.EXPECT().FindOne(gomock.Any(), gomock.Any()).Return(mockSingleResultUpdate),
		mockSingleResultUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(&LoadBalanceQuota{
				Id:    idUpdateSuccess,
				Quota: 0,
			}))
			return nil
		}),
	)
	inUpdateSuccess := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: int64(loadBalanceCountUpdateSuccess),
	}
	// success insert but decode failed
	mockCollectionInsertSuccessDecodeFailed := mockMongo.NewMockCollection(ctrl)
	mockSingleResultInsertSuccessDecodeFailed := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxInsertSuccessDecodeFailed := context.Background()
	loadBalanceCountInsertSuccessDecodeFailed := 10
	resultInsertSuccessDecodeFailed := pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	dataInsertSuccessDecodeFailed := &LoadBalanceQuota{
		Quota: int64(loadBalanceCountInsertSuccessDecodeFailed),
	}
	mockCollectionInsertSuccessDecodeFailed.EXPECT().InsertOne(ctxInsertSuccessDecodeFailed, dataInsertSuccessDecodeFailed).Return(&resultInsertSuccessDecodeFailed, nil)
	gomock.InOrder(
		mockCollectionInsertSuccessDecodeFailed.EXPECT().FindOne(gomock.Any(), gomock.Any()).Return(mockSingleResultInsertSuccessDecodeFailed),
		mockSingleResultInsertSuccessDecodeFailed.EXPECT().Decode(gomock.Any()).Return(pkgMongo.ErrWrongClient),
	)
	inUpdateSuccessDecodeFailed := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: int64(loadBalanceCountInsertSuccessDecodeFailed),
	}
	// failed insert
	mockCollectionInsertFailed := mockMongo.NewMockCollection(ctrl)
	ctxFailed := context.Background()
	loadBalanceCountFailed := 10
	mockCollectionInsertFailed.EXPECT().InsertOne(ctxFailed, &LoadBalanceQuota{
		Quota: int64(loadBalanceCountFailed),
	}).Return(nil, pkgMongo.ErrWrongClient)
	inFailed := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: int64(loadBalanceCountFailed),
	}
	gomock.InOrder(
		mockCollectionInsertFailed.EXPECT().FindOne(gomock.Any(), gomock.Any()).Return(mockSingleResult),
		mockSingleResult.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(&LoadBalanceQuota{
				Quota: 0,
			}))
			return nil
		}),
	)
	// failed update
	mockCollectionUpdateFailed := mockMongo.NewMockCollection(ctrl)
	mockSingleResultUpdateFailed := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxUpdateFailed := context.Background()
	loadBalanceCountUpdateFailed := 10
	idUpdateFailed := primitive.NewObjectID()
	resultUpdateFailed := pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
		UpsertedCount: 0,
	}
	inUpdateFailed := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: int64(loadBalanceCountUpdateFailed),
	}
	mockCollectionUpdateFailed.EXPECT().UpdateOne(ctxUpdateFailed, bson.M{"_id": idUpdateFailed}, gomock.Any()).Return(&resultUpdateFailed, pkgMongo.ErrWrongClient)
	gomock.InOrder(
		mockCollectionUpdateFailed.EXPECT().FindOne(gomock.Any(), gomock.Any()).Return(mockSingleResultUpdateFailed),
		mockSingleResultUpdateFailed.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(&LoadBalanceQuota{
				Id:    idUpdateFailed,
				Quota: 0,
			}))
			return nil
		}),
	)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		in  *load_balance.RequestLoadBalance
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success insert",
			fields: fields{
				Collection: mockCollectionInsertSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
		},
		{
			name: "success update",
			fields: fields{
				Collection: mockCollectionUpdateSuccess,
			},
			args: args{
				ctx: ctxUpdateSuccess,
				in:  inUpdateSuccess,
			},
		},
		{
			name: "success insert but decode failed",
			fields: fields{
				Collection: mockCollectionInsertSuccessDecodeFailed,
			},
			args: args{
				ctx: ctxInsertSuccessDecodeFailed,
				in:  inUpdateSuccessDecodeFailed,
			},
		},
		{
			name: "failed insert",
			fields: fields{
				Collection: mockCollectionInsertFailed,
			},
			args: args{
				ctx: ctxFailed,
				in:  inFailed,
			},
			wantErr: true,
		},
		{
			name: "failed update",
			fields: fields{
				Collection: mockCollectionUpdateFailed,
			},
			args: args{
				ctx: ctxUpdateFailed,
				in:  inUpdateFailed,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if err := m.CreateQuotaLoadBalance(tt.args.ctx, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("CreateQuotaLoadBalance() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoContext_GetLoadBalanceQuota(t *testing.T) {
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   LoadBalanceQuota
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if got := m.GetLoadBalanceQuota(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetLoadBalanceQuota() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoContext_GetLoadBalanceQuota1(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mockCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	mockSingleResultSuccess := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	loadBalanceQuotaSuccess := &LoadBalanceQuota{
		Id:    primitive.NewObjectID(),
		Quota: 10,
	}
	gomock.InOrder(
		mockCollectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(mockSingleResultSuccess),
		mockSingleResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(loadBalanceQuotaSuccess))
			return nil
		}),
	)

	// error
	mockCollectionFailed := mockMongo.NewMockCollection(ctrl)
	mockSingleResultFailed := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxFailed := context.Background()
	gomock.InOrder(
		mockCollectionFailed.EXPECT().FindOne(ctxFailed, bson.M{}).Return(mockSingleResultFailed),
		mockSingleResultFailed.EXPECT().Decode(gomock.Any()).Return(pkgMongo.ErrNilDocument),
	)
	loadBalanceQuotaFailed := &LoadBalanceQuota{}
	// empty
	mockCollectionEmpty := mockMongo.NewMockCollection(ctrl)
	mockSingleResultEmpty := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxEmpty := context.Background()
	loadBalanceQuotaEmpty := &LoadBalanceQuota{}
	gomock.InOrder(
		mockCollectionEmpty.EXPECT().FindOne(ctxEmpty, bson.M{}).Return(mockSingleResultEmpty),
		mockSingleResultEmpty.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(loadBalanceQuotaEmpty))
			return nil
		}),
	)
	loadBalanceQuotaEmpty = &LoadBalanceQuota{}

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *LoadBalanceQuota
	}{
		{
			name: "success",
			fields: fields{
				Collection: mockCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want: loadBalanceQuotaSuccess,
		},
		{
			name: "failed",
			fields: fields{
				Collection: mockCollectionFailed,
			},
			args: args{
				ctx: ctxFailed,
			},
			want: loadBalanceQuotaFailed,
		},
		{
			name: "empty",
			fields: fields{
				Collection: mockCollectionEmpty,
			},
			args: args{
				ctx: ctxEmpty,
			},
			want: loadBalanceQuotaEmpty,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if got := m.GetLoadBalanceQuota(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetLoadBalanceQuota() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewMongoRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockCollection := mockMongo.NewMockCollection(ctrl)
	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want RepositoryLoadBalance
	}{
		{
			name: "success",
			args: args{
				collection: mockCollection,
			},
			want: &mongoContext{
				Collection: mockCollection,
			},
		},
		{
			name: "success empty",
			args: args{
				collection: nil,
			},
			want: &mongoContext{
				Collection: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
