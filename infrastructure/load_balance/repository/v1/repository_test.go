package v1

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestLoadBalanceQuota_UsagePercent(t *testing.T) {
	quota := 100
	usage := 50
	percent := float32(50)
	// zero
	quotaZero := 0
	usageZero := 0
	percentZero := float32(0)
	// quota zero
	quotaZero2 := 0
	usageZero2 := 50
	percentZero2 := float32(0)
	// usage zero
	quotaZero3 := 100
	usageZero3 := 0
	percentZero3 := float32(0)
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Usage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Usage Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(quota),
				Usage: int64(usage),
			},
			want: percent,
		},
		{
			name: "Test Usage Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(quotaZero),
				Usage: int64(usageZero),
			},
			want: percentZero,
		},
		{
			name: "Test Usage Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(quotaZero2),
				Usage: int64(usageZero2),
			},
			want: percentZero2,
		},
		{
			name: "Test Usage Percent Usage Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(quotaZero3),
				Usage: int64(usageZero3),
			},
			want: percentZero3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := LoadBalanceQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Usage: tt.fields.Usage,
			}
			if got := q.UsagePercent(); got != tt.want {
				t.Errorf("UsagePercent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoadBalanceQuota_FreePercent(t *testing.T) {
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Usage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Free Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(100),
				Usage: int64(50),
			},
			want: float32(50),
		},
		{
			name: "Test Free Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(0),
				Usage: int64(0),
			},
			want: float32(0),
		},
		{
			name: "Test Free Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(0),
				Usage: int64(50),
			},
			want: float32(0),
		},
		{
			name: "Test Free Percent Usage Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: int64(100),
				Usage: int64(0),
			},
			want: float32(100),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := LoadBalanceQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Usage: tt.fields.Usage,
			}
			if got := q.FreePercent(); got != tt.want {
				t.Errorf("FreePercent() = %v, want %v", got, tt.want)
			}
		})
	}
}
