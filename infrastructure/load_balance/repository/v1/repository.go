package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"moi-quota/pkg/api/infrastructure/v1/load_balance"
)

type LoadBalanceQuota struct {
	Id    primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Quota int64              `json:"quota" bson:"quota"`
	Usage int64              `json:"usage" bson:"usage"`
}

func (q LoadBalanceQuota) UsagePercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return float32(q.Usage) / float32(q.Quota) * 100
}

func (q LoadBalanceQuota) FreePercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return 100 - q.UsagePercent()
}

type RepositoryLoadBalance interface {
	CreateQuotaLoadBalance(ctx context.Context, in *load_balance.RequestLoadBalance) error
	GetLoadBalanceQuota(ctx context.Context) *LoadBalanceQuota
}
