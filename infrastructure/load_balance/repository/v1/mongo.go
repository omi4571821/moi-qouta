package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"moi-quota/driver/mongo"
	"moi-quota/pkg/api/infrastructure/v1/load_balance"
)

const CollectionName = "quota_load_balance"

type mongoContext struct {
	mongo.Collection
}

func (m mongoContext) CreateQuotaLoadBalance(ctx context.Context, in *load_balance.RequestLoadBalance) error {
	cursor := m.FindOne(ctx, bson.M{})
	currentQuota := &LoadBalanceQuota{
		Quota: 0,
	}
	err := cursor.Decode(&currentQuota)
	currentQuota.Quota += in.RequestLoadBalanceCount
	if currentQuota.Id.IsZero() {
		_, err = m.InsertOne(ctx, currentQuota)
	} else {
		_, err = m.UpdateOne(ctx, bson.M{"_id": currentQuota.Id}, bson.M{"$set": currentQuota})
	}
	return err
}

func (m mongoContext) GetLoadBalanceQuota(ctx context.Context) *LoadBalanceQuota {
	cursor := m.FindOne(ctx, bson.M{})
	var currentQuota *LoadBalanceQuota
	err := cursor.Decode(&currentQuota)
	if err != nil {
		return &LoadBalanceQuota{}
	}
	return currentQuota
}

func NewMongoRepository(collection mongo.Collection) RepositoryLoadBalance {
	return &mongoContext{collection}
}
