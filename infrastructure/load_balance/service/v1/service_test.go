package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	repository "moi-quota/infrastructure/load_balance/repository/v1"
	repositoryMock "moi-quota/mock/infrastructure/load_balance/repository/v1"
	"moi-quota/pkg/api/infrastructure/v1/load_balance"
	protocolV1 "moi-quota/pkg/protocol/v1"
	"reflect"
	"testing"
)

func Test_loadBalanceContext_SetLoadBalance(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	ctxSuccess := context.Background()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	inSuccess := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: 10,
	}
	quotaSuccess := &repository.LoadBalanceQuota{
		Quota: 100,
		Usage: 50,
	}
	repositorySuccess.EXPECT().CreateQuotaLoadBalance(ctxSuccess, inSuccess).Return(nil)
	repositorySuccess.EXPECT().GetLoadBalanceQuota(ctxSuccess).Return(quotaSuccess)

	wantSuccess := &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        quotaSuccess.Quota,
		LoadBalanceUsage:        quotaSuccess.Usage,
		LoadBalanceUsagePercent: quotaSuccess.UsagePercent(),
		LoadBalanceFreePercent:  quotaSuccess.FreePercent(),
	}
	// error case insert
	ctxErrorInsert := context.Background()
	repositoryErrorInsert := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	inErrorInsert := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: 10,
	}
	errInsert := primitive.ErrInvalidHex
	repositoryErrorInsert.EXPECT().CreateQuotaLoadBalance(ctxErrorInsert, inErrorInsert).Return(errInsert)
	// success empty
	ctxSuccessEmpty := context.Background()
	repositorySuccessEmpty := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	inSuccessEmpty := &load_balance.RequestLoadBalance{
		RequestLoadBalanceCount: 10,
	}
	quotaSuccessEmpty := &repository.LoadBalanceQuota{}
	repositorySuccessEmpty.EXPECT().CreateQuotaLoadBalance(ctxSuccessEmpty, inSuccessEmpty).Return(nil)
	repositorySuccessEmpty.EXPECT().GetLoadBalanceQuota(ctxSuccessEmpty).Return(quotaSuccessEmpty)
	wantSuccessEmpty := &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        quotaSuccessEmpty.Quota,
		LoadBalanceUsage:        quotaSuccessEmpty.Usage,
		LoadBalanceUsagePercent: quotaSuccessEmpty.UsagePercent(),
		LoadBalanceFreePercent:  quotaSuccessEmpty.FreePercent(),
	}

	type fields struct {
		UnimplementedLoadBalanceServiceServer load_balance.UnimplementedLoadBalanceServiceServer
		repositoryLoadBalance                 repository.RepositoryLoadBalance
	}
	type args struct {
		ctx context.Context
		in  *load_balance.RequestLoadBalance
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *load_balance.ResponseLoadBalance
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "success empty",
			fields: fields{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositorySuccessEmpty,
			},
			args: args{
				ctx: ctxSuccessEmpty,
				in:  inSuccessEmpty,
			},
			want: wantSuccessEmpty,
		},
		{
			name: "error insert",
			fields: fields{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositoryErrorInsert,
			},
			args: args{
				ctx: ctxErrorInsert,
				in:  inErrorInsert,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loadBalanceContext{
				UnimplementedLoadBalanceServiceServer: tt.fields.UnimplementedLoadBalanceServiceServer,
				repositoryLoadBalance:                 tt.fields.repositoryLoadBalance,
			}
			got, err := l.SetLoadBalance(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("SetLoadBalance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SetLoadBalance() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_loadBalanceContext_GetLoadBalance(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// success case
	ctxSuccess := context.Background()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	qSuccess := &repository.LoadBalanceQuota{
		Quota: 100,
		Usage: 50,
	}
	repositorySuccess.EXPECT().GetLoadBalanceQuota(ctxSuccess).Return(qSuccess)
	wantSuccess := &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        qSuccess.Quota,
		LoadBalanceUsage:        qSuccess.Usage,
		LoadBalanceUsagePercent: qSuccess.UsagePercent(),
		LoadBalanceFreePercent:  qSuccess.FreePercent(),
	}
	// empty case
	ctxEmpty := context.Background()
	repositoryEmpty := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	qEmpty := &repository.LoadBalanceQuota{}
	repositoryEmpty.EXPECT().GetLoadBalanceQuota(ctxEmpty).Return(qEmpty)
	wantEmpty := &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        qEmpty.Quota,
		LoadBalanceUsage:        qEmpty.Usage,
		LoadBalanceUsagePercent: qEmpty.UsagePercent(),
		LoadBalanceFreePercent:  qEmpty.FreePercent(),
	}
	type fields struct {
		UnimplementedLoadBalanceServiceServer load_balance.UnimplementedLoadBalanceServiceServer
		repositoryLoadBalance                 repository.RepositoryLoadBalance
	}
	type args struct {
		ctx context.Context
		in  *emptypb.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *load_balance.ResponseLoadBalance
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  &emptypb.Empty{},
			},
			want: wantSuccess,
		},
		{
			name: "empty",
			fields: fields{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositoryEmpty,
			},
			args: args{
				ctx: ctxEmpty,
				in:  &emptypb.Empty{},
			},
			want: wantEmpty,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loadBalanceContext{
				UnimplementedLoadBalanceServiceServer: tt.fields.UnimplementedLoadBalanceServiceServer,
				repositoryLoadBalance:                 tt.fields.repositoryLoadBalance,
			}
			got, err := l.GetLoadBalance(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLoadBalance() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetLoadBalance() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_loadBalanceService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	ctxSuccess := context.Background()
	type fields struct {
		service load_balance.LoadBalanceServiceServer
		ctx     context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				service: &loadBalanceContext{
					UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
					repositoryLoadBalance:                 repositorySuccess,
				},
				ctx: ctxSuccess,
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loadBalanceService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			l.TcpListening(tt.args.server)
		})
	}
}

func Test_loadBalanceService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	ctxSuccess := context.Background()
	type fields struct {
		service load_balance.LoadBalanceServiceServer
		ctx     context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				service: &loadBalanceContext{
					UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
					repositoryLoadBalance:                 repositorySuccess,
				},
				ctx: ctxSuccess,
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := loadBalanceService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			if err := l.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initLoadBalanceServiceServer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	type args struct {
		loadBalanceRepository repository.RepositoryLoadBalance
	}
	tests := []struct {
		name string
		args args
		want load_balance.LoadBalanceServiceServer
	}{
		{
			name: "success",
			args: args{
				loadBalanceRepository: repositorySuccess,
			},
			want: &loadBalanceContext{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				repositoryLoadBalance:                 repositorySuccess,
			},
		},
		{
			name: "empty",
			args: args{
				loadBalanceRepository: nil,
			},
			want: &loadBalanceContext{
				UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initLoadBalanceServiceServer(tt.args.loadBalanceRepository); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initLoadBalanceServiceServer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewProtocolService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repositorySuccess := repositoryMock.NewMockRepositoryLoadBalance(ctrl)
	ctxSuccess := context.Background()

	type args struct {
		ctx                   context.Context
		loadBalanceRepository repository.RepositoryLoadBalance
	}
	tests := []struct {
		name string
		args args
		want protocolV1.Service
	}{
		{
			name: "success",
			args: args{
				ctx:                   ctxSuccess,
				loadBalanceRepository: repositorySuccess,
			},
			want: &loadBalanceService{
				service: &loadBalanceContext{
					UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
					repositoryLoadBalance:                 repositorySuccess,
				},
				ctx: ctxSuccess,
			},
		},
		{
			name: "empty",
			args: args{
				ctx: ctxSuccess,
			},
			want: &loadBalanceService{
				service: &loadBalanceContext{
					UnimplementedLoadBalanceServiceServer: load_balance.UnimplementedLoadBalanceServiceServer{},
				},
				ctx: ctxSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewProtocolService(tt.args.ctx, tt.args.loadBalanceRepository); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProtocolService() = %v, want %v", got, tt.want)
			}
		})
	}
}
