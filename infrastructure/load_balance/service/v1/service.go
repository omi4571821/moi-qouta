package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	repository "moi-quota/infrastructure/load_balance/repository/v1"
	"moi-quota/pkg/api/infrastructure/v1/load_balance"
	protocol "moi-quota/pkg/protocol/v1"
)

type loadBalanceContext struct {
	load_balance.UnimplementedLoadBalanceServiceServer
	repositoryLoadBalance repository.RepositoryLoadBalance
}

func (l loadBalanceContext) SetLoadBalance(ctx context.Context, in *load_balance.RequestLoadBalance) (*load_balance.ResponseLoadBalance, error) {
	var err = l.repositoryLoadBalance.CreateQuotaLoadBalance(ctx, in)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "method SetLoadBalance not implemented")
	}
	loadBalanceQuota := l.repositoryLoadBalance.GetLoadBalanceQuota(ctx)
	return &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        loadBalanceQuota.Quota,
		LoadBalanceUsage:        loadBalanceQuota.Usage,
		LoadBalanceUsagePercent: loadBalanceQuota.UsagePercent(),
		LoadBalanceFreePercent:  loadBalanceQuota.FreePercent(),
	}, nil
}
func (l loadBalanceContext) GetLoadBalance(ctx context.Context, in *emptypb.Empty) (*load_balance.ResponseLoadBalance, error) {
	loadBalanceQuota := l.repositoryLoadBalance.GetLoadBalanceQuota(ctx)
	return &load_balance.ResponseLoadBalance{
		LoadBalanceQuota:        loadBalanceQuota.Quota,
		LoadBalanceUsage:        loadBalanceQuota.Usage,
		LoadBalanceUsagePercent: loadBalanceQuota.UsagePercent(),
		LoadBalanceFreePercent:  loadBalanceQuota.FreePercent(),
	}, nil
}

type loadBalanceService struct {
	service load_balance.LoadBalanceServiceServer
	ctx     context.Context
}

func (l loadBalanceService) TcpListening(server *grpc.Server) {
	load_balance.RegisterLoadBalanceServiceServer(server, l.service)
}

func (l loadBalanceService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return load_balance.RegisterLoadBalanceServiceHandler(l.ctx, mux, conn)
}

func initLoadBalanceServiceServer(loadBalanceRepository repository.RepositoryLoadBalance) load_balance.LoadBalanceServiceServer {
	return &loadBalanceContext{
		repositoryLoadBalance: loadBalanceRepository,
	}
}

func NewProtocolService(ctx context.Context, loadBalanceRepository repository.RepositoryLoadBalance) protocol.Service {
	return &loadBalanceService{
		service: initLoadBalanceServiceServer(loadBalanceRepository),
		ctx:     ctx,
	}
}
