package instance

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type QuotaInstanceType string

const QuotaType QuotaInstanceType = "quota"
const UsageType QuotaInstanceType = "usage"

type QuotaInstance struct {
	Id         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Cpu        int64              `json:"cpu" bson:"cpu"`
	Mem        int64              `json:"mem" bson:"mem"`
	Disk       int64              `json:"disk" bson:"disk"`
	SelectType QuotaInstanceType  `json:"select_type" bson:"select_type"`
	TotalVM    int64              `json:"total_vm" bson:"total_vm"`
}

func (t QuotaInstanceType) String() string {
	return string(t)
}

type VirtualInstanceRepository interface {
	CalculateInstance(ctx context.Context) []*QuotaInstance
	AddInstance(ctx context.Context, in *QuotaInstance, quotaType QuotaInstanceType) error
}
