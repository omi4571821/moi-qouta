package instance

import "testing"

func TestQuotaInstanceType_String(t *testing.T) {
	tests := []struct {
		name string
		t    QuotaInstanceType
		want string
	}{
		{
			name: "success quota",
			t:    QuotaType,
			want: "quota",
		},
		{
			name: "success usage",
			t:    UsageType,
			want: "usage",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
