package instance

import (
	"context"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	mockMongo "moi-quota/mock/driver/mongo"
	"reflect"
	"testing"
)

func Test_mongoInstance_AddInstance(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	inputSuccess := &QuotaInstance{
		Cpu:     1,
		Mem:     1,
		Disk:    1,
		TotalVM: 1,
	}
	resultInsertOneSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	quotaInstanceTypeSuccess := QuotaType
	mongoCollectionSuccess.EXPECT().InsertOne(ctxSuccess, &QuotaInstance{
		Cpu:        inputSuccess.Cpu,
		Mem:        inputSuccess.Mem,
		Disk:       inputSuccess.Disk,
		SelectType: quotaInstanceTypeSuccess,
		TotalVM:    inputSuccess.TotalVM,
	}).Return(resultInsertOneSuccess, nil)
	// fail case
	mongoCollectionFail := mockMongo.NewMockCollection(ctrl)
	ctxFail := context.Background()
	inputFail := &QuotaInstance{
		Cpu:     1,
		Mem:     1,
		Disk:    1,
		TotalVM: 1,
	}
	resultInsertOneFail := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	resultInsertOneFail = nil
	quotaInstanceTypeFail := QuotaType
	mongoCollectionFail.EXPECT().InsertOne(ctxFail, &QuotaInstance{
		Cpu:        inputFail.Cpu,
		Mem:        inputFail.Mem,
		Disk:       inputFail.Disk,
		SelectType: quotaInstanceTypeFail,
		TotalVM:    inputFail.TotalVM,
	}).Return(resultInsertOneFail, primitive.ErrInvalidHex)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx       context.Context
		in        *QuotaInstance
		quotaType QuotaInstanceType
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: mongoCollectionSuccess,
			},
			args: args{
				ctx:       ctxSuccess,
				in:        inputSuccess,
				quotaType: quotaInstanceTypeSuccess,
			},
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				Collection: mongoCollectionFail,
			},
			args: args{
				ctx:       ctxFail,
				in:        inputFail,
				quotaType: quotaInstanceTypeFail,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mongoInstance{
				Collection: tt.fields.Collection,
			}
			if err := m.AddInstance(tt.args.ctx, tt.args.in, tt.args.quotaType); (err != nil) != tt.wantErr {
				t.Errorf("AddInstance() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoInstance_CalculateInstance(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	ctxSuccess := context.Background()
	mongoCursorSuccess := mockMongo.NewMockCursorResultDecoder(ctrl)
	resultSuccess := []*SumQuotaInstance{
		{
			Id:      QuotaType.String(),
			Cpu:     120,
			Mem:     10000,
			Disk:    10000000,
			TotalVM: 1000,
		},
		{
			Id:      UsageType.String(),
			Cpu:     60,
			Mem:     100,
			Disk:    10000,
			TotalVM: 300,
		},
	}
	gomock.InOrder(
		mongoCollectionSuccess.EXPECT().Aggregate(ctxSuccess, bson.A{
			bson.M{
				"$group": bson.M{
					"_id":      "$select_type",
					"cpu":      bson.M{"$sum": "$cpu"},
					"mem":      bson.M{"$sum": "$mem"},
					"disk":     bson.M{"$sum": "$disk"},
					"total_vm": bson.M{"$sum": 1},
				},
			},
		}).Return(mongoCursorSuccess, nil),
		mongoCursorSuccess.EXPECT().All(gomock.Any(), gomock.Any()).DoAndReturn(func(ctx context.Context, i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(resultSuccess))
			return nil
		}),
	)
	wantSuccess := []*QuotaInstance{
		{
			Cpu:        120,
			Mem:        10000,
			Disk:       10000000,
			TotalVM:    1000,
			SelectType: QuotaType,
		},
		{
			Cpu:        60,
			Mem:        100,
			Disk:       10000,
			TotalVM:    300,
			SelectType: UsageType,
		},
	}
	// fail case aggregate
	mongoCollectionFailAggregate := mockMongo.NewMockCollection(ctrl)
	ctxFailAggregate := context.Background()
	mongoCollectionFailAggregate.EXPECT().Aggregate(ctxFailAggregate, bson.A{
		bson.M{
			"$group": bson.M{
				"_id":      "$select_type",
				"cpu":      bson.M{"$sum": "$cpu"},
				"mem":      bson.M{"$sum": "$mem"},
				"disk":     bson.M{"$sum": "$disk"},
				"total_vm": bson.M{"$sum": 1},
			},
		},
	}).Return(nil, pkgMongo.ErrNilCursor)
	wantFailAggregate := []*QuotaInstance{}
	wantFailAggregate = nil
	// fail case all
	mongoCollectionFailAll := mockMongo.NewMockCollection(ctrl)
	ctxFailAll := context.Background()
	mongoCursorFailAll := mockMongo.NewMockCursorResultDecoder(ctrl)
	gomock.InOrder(
		mongoCollectionFailAll.EXPECT().Aggregate(ctxFailAll, bson.A{
			bson.M{
				"$group": bson.M{
					"_id":      "$select_type",
					"cpu":      bson.M{"$sum": "$cpu"},
					"mem":      bson.M{"$sum": "$mem"},
					"disk":     bson.M{"$sum": "$disk"},
					"total_vm": bson.M{"$sum": 1},
				},
			},
		}).Return(mongoCursorFailAll, nil),
		mongoCursorFailAll.EXPECT().All(gomock.Any(), gomock.Any()).Return(pkgMongo.ErrNoDocuments),
	)
	wantFailAll := []*QuotaInstance{}
	wantFailAll = nil
	// data empty
	mongoCollectionDataEmpty := mockMongo.NewMockCollection(ctrl)
	ctxDataEmpty := context.Background()
	mongoCursorDataEmpty := mockMongo.NewMockCursorResultDecoder(ctrl)
	resultDataEmpty := []*SumQuotaInstance{}
	gomock.InOrder(
		mongoCollectionDataEmpty.EXPECT().Aggregate(ctxDataEmpty, bson.A{
			bson.M{
				"$group": bson.M{
					"_id":      "$select_type",
					"cpu":      bson.M{"$sum": "$cpu"},
					"mem":      bson.M{"$sum": "$mem"},
					"disk":     bson.M{"$sum": "$disk"},
					"total_vm": bson.M{"$sum": 1},
				},
			},
		}).Return(mongoCursorDataEmpty, nil),
		mongoCursorDataEmpty.EXPECT().All(gomock.Any(), gomock.Any()).DoAndReturn(func(ctx context.Context, i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(resultDataEmpty))
			return nil
		}),
	)
	var wantDataEmpty []*QuotaInstance

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []*QuotaInstance
	}{
		{
			name: "success",
			fields: fields{
				Collection: mongoCollectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "fail aggregate",
			fields: fields{
				Collection: mongoCollectionFailAggregate,
			},
			args: args{
				ctx: ctxFailAggregate,
			},
			want: wantFailAggregate,
		},
		{
			name: "fail all",
			fields: fields{
				Collection: mongoCollectionFailAll,
			},
			args: args{
				ctx: ctxFailAll,
			},
			want: wantFailAll,
		},
		{
			name: "data empty",
			fields: fields{
				Collection: mongoCollectionDataEmpty,
			},
			args: args{
				ctx: ctxDataEmpty,
			},
			want: wantDataEmpty,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mongoInstance{
				Collection: tt.fields.Collection,
			}
			if got := m.CalculateInstance(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CalculateInstance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewMongoInstanceRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)

	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want VirtualInstanceRepository
	}{
		{
			name: "success",
			args: args{
				collection: nil,
			},
			want: &mongoInstance{
				Collection: nil,
			},
		},
		{
			name: "success with collection",
			args: args{
				collection: mongoCollectionSuccess,
			},
			want: &mongoInstance{
				Collection: mongoCollectionSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoInstanceRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoInstanceRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
