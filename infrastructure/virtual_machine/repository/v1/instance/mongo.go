package instance

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	mongoDriver "moi-quota/driver/mongo"
)

const CollectionName = "instances"

type mongoInstance struct {
	mongoDriver.Collection
}
type SumQuotaInstance struct {
	Id      string `bson:"_id"`
	Cpu     int64  `bson:"cpu"`
	Mem     int64  `bson:"mem"`
	Disk    int64  `bson:"disk"`
	TotalVM int64  `bson:"total_vm"`
}

func (m *mongoInstance) CalculateInstance(ctx context.Context) []*QuotaInstance {
	var result []*SumQuotaInstance
	var cursor, err = m.Aggregate(ctx, bson.A{
		bson.M{
			"$group": bson.M{
				"_id":      "$select_type",
				"cpu":      bson.M{"$sum": "$cpu"},
				"mem":      bson.M{"$sum": "$mem"},
				"disk":     bson.M{"$sum": "$disk"},
				"total_vm": bson.M{"$sum": 1},
			},
		},
	})
	if err != nil {
		return nil
	}
	err = cursor.All(ctx, &result)
	if err != nil {
		return nil
	}
	var instances []*QuotaInstance
	for _, data := range result {
		instances = append(instances, &QuotaInstance{
			SelectType: QuotaInstanceType(data.Id),
			Cpu:        data.Cpu,
			Mem:        data.Mem,
			Disk:       data.Disk,
			TotalVM:    data.TotalVM,
		})
	}
	return instances
}

func (m *mongoInstance) AddInstance(ctx context.Context, in *QuotaInstance, quotaType QuotaInstanceType) error {
	in.SelectType = quotaType
	_, err := m.InsertOne(ctx, in)
	return err
}

func NewMongoInstanceRepository(collection mongoDriver.Collection) VirtualInstanceRepository {
	return &mongoInstance{collection}
}
