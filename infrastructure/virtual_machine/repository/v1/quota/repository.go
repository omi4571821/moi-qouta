package quota

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"moi-quota/pkg/api/infrastructure/v1/virtual_machine"
)

type QuotaVM struct {
	Id               primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	TotalCPU         int64              `json:"total_cpu" bson:"total_cpu"`
	TotalMemory      int64              `json:"total_memory" bson:"total_memory"`
	TotalStorage     int64              `json:"total_storage" bson:"total_storage"`
	TotalCPUUsed     int64              `json:"total_cpu_used" bson:"total_cpu_used"`
	TotalMemoryUsed  int64              `json:"total_memory_used" bson:"total_memory_used"`
	TotalStorageUsed int64              `json:"total_storage_used" bson:"total_storage_used"`
	TotalVMUsed      int64              `json:"total_vm_used" bson:"total_vm_used"`
}

func (v QuotaVM) GetMemoryPercentUsed() float32 {
	if v.TotalMemory == 0 {
		return 0
	}
	return float32(v.TotalMemoryUsed) / float32(v.TotalMemory) * 100
}

func (v QuotaVM) GetCpuPercentUsed() float32 {
	if v.TotalCPU == 0 {
		return 0
	}
	return float32(v.TotalCPUUsed) / float32(v.TotalCPU) * 100
}

func (v QuotaVM) GetStoragePercentUsed() float32 {
	if v.TotalStorage == 0 {
		return 0
	}
	return float32(v.TotalStorageUsed) / float32(v.TotalStorage) * 100
}

func (v QuotaVM) GetMemoryPercentFree() float32 {
	if v.TotalMemory == 0 {
		return 0
	}
	return 100 - v.GetMemoryPercentUsed()
}

func (v QuotaVM) GetCpuPercentFree() float32 {
	if v.TotalCPU == 0 {
		return 0
	}
	return 100 - v.GetCpuPercentUsed()
}

func (v QuotaVM) GetStoragePercentFree() float32 {
	if v.TotalStorage == 0 {
		return 0
	}
	return 100 - v.GetStoragePercentUsed()
}

type VirtualRepository interface {
	AddVirtualMachine(ctx context.Context, in *virtual_machine.RequestQuotaVM) error
	GetReportQuotaVirtualMachine(ctx context.Context) *QuotaVM
	UpdateReportQuotaVirtualMachine(ctx context.Context) error
}
