package quota

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	mongoDriver "moi-quota/driver/mongo"
	"moi-quota/infrastructure/virtual_machine/repository/v1/instance"
	"moi-quota/pkg/api/infrastructure/v1/virtual_machine"
)

const CollectionName = "quota_vm"

type mongoContext struct {
	mongoDriver.Collection
	instanceRepository instance.VirtualInstanceRepository
}

func (m *mongoContext) UpdateReportQuotaVirtualMachine(ctx context.Context) error {
	var quotaVM = &QuotaVM{}
	cursor := m.FindOne(ctx, bson.M{})
	err := cursor.Decode(&quotaVM)
	var instances = m.instanceRepository.CalculateInstance(ctx)
	var totalCpu, totalMem, totalDisk int64
	var totalCpuUsed, totalMemUsed, totalDiskUsed int64
	var TotalVMUsed int64
	for _, data := range instances {
		if data.SelectType == instance.QuotaType {
			totalCpu += data.Cpu
			totalMem += data.Mem
			totalDisk += data.Disk
		}
		if data.SelectType == instance.UsageType {
			TotalVMUsed += data.TotalVM
			totalCpuUsed += data.Cpu
			totalMemUsed += data.Mem
			totalDiskUsed += data.Disk
		}
	}

	quotaVM.TotalCPU = totalCpu
	quotaVM.TotalMemory = totalMem
	quotaVM.TotalStorage = totalDisk
	quotaVM.TotalCPUUsed = totalCpuUsed
	quotaVM.TotalMemoryUsed = totalMemUsed
	quotaVM.TotalStorageUsed = totalDiskUsed
	quotaVM.TotalVMUsed = TotalVMUsed
	if quotaVM.Id.IsZero() {
		_, err = m.InsertOne(ctx, quotaVM)
		return err
	}
	_, err = m.UpdateOne(ctx, bson.M{"_id": quotaVM.Id}, bson.M{"$set": quotaVM})
	return err
}

func (m *mongoContext) AddVirtualMachine(ctx context.Context, in *virtual_machine.RequestQuotaVM) error {
	vm := &instance.QuotaInstance{
		Cpu:        in.RequestCpu,
		Mem:        in.RequestMemory,
		Disk:       in.RequestStorage,
		SelectType: instance.QuotaType,
	}
	var err = m.instanceRepository.AddInstance(ctx, vm, instance.QuotaType)
	if err != nil {
		return err
	}
	return m.UpdateReportQuotaVirtualMachine(ctx)
}

func (m *mongoContext) GetReportQuotaVirtualMachine(ctx context.Context) *QuotaVM {
	var quotaVM = &QuotaVM{}
	cursor := m.FindOne(ctx, bson.M{})
	err := cursor.Decode(&quotaVM)
	if err != nil {
		return nil
	}
	return quotaVM
}

func NewRepository(collection mongoDriver.Collection, instanceRepository instance.VirtualInstanceRepository) VirtualRepository {
	return &mongoContext{collection, instanceRepository}
}
