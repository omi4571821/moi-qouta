package quota

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	"moi-quota/infrastructure/virtual_machine/repository/v1/instance"
	mockMongo "moi-quota/mock/driver/mongo"
	instanceMock "moi-quota/mock/infrastructure/virtual_machine/repository/v1/instance"
	"moi-quota/pkg/api/infrastructure/v1/virtual_machine"
	"reflect"
	"testing"
)

func Test_mongoContext_UpdateReportQuotaVirtualMachine(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// success case
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultSuccess := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	instanceRepositorySuccess := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaSuccess := []*instance.QuotaInstance{
		{
			Cpu:        100,
			Mem:        1000,
			Disk:       10000,
			TotalVM:    0,
			SelectType: instance.QuotaType,
		},
		{
			Cpu:        10,
			Mem:        10,
			Disk:       500,
			TotalVM:    10,
			SelectType: instance.UsageType,
		},
	}
	instanceRepositorySuccess.EXPECT().CalculateInstance(ctxSuccess).Return(instanceQuotaSuccess)
	var quotaVM = &QuotaVM{
		TotalCPU:         instanceQuotaSuccess[0].Cpu,
		TotalMemory:      instanceQuotaSuccess[0].Mem,
		TotalStorage:     instanceQuotaSuccess[0].Disk,
		TotalCPUUsed:     instanceQuotaSuccess[1].Cpu,
		TotalMemoryUsed:  instanceQuotaSuccess[1].Mem,
		TotalStorageUsed: instanceQuotaSuccess[1].Disk,
		TotalVMUsed:      instanceQuotaSuccess[1].TotalVM,
	}
	resultInsertOneSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	mongoCollectionSuccess.EXPECT().InsertOne(ctxSuccess, quotaVM).Return(resultInsertOneSuccess, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(mongoSingleResultSuccess),
		mongoSingleResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{}))
			return nil
		}),
	)
	// update success
	mongoCollectionUpdateSuccess := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultUpdateSuccess := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxUpdateSuccess := context.Background()
	instanceRepositoryUpdateSuccess := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaUpdateSuccess := []*instance.QuotaInstance{
		{
			Cpu:        100,
			Mem:        1000,
			Disk:       10000,
			TotalVM:    0,
			SelectType: instance.QuotaType,
		},
		{
			Cpu:        10,
			Mem:        10,
			Disk:       500,
			TotalVM:    10,
			SelectType: instance.UsageType,
		},
	}
	instanceRepositoryUpdateSuccess.EXPECT().CalculateInstance(ctxUpdateSuccess).Return(instanceQuotaUpdateSuccess)
	idUpdateSuccess := primitive.NewObjectID()
	resultUpdateSuccess := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
		UpsertedCount: 1,
		UpsertedID:    nil,
	}
	mongoCollectionUpdateSuccess.EXPECT().UpdateOne(ctxUpdateSuccess, bson.M{"_id": idUpdateSuccess}, gomock.Any()).Return(resultUpdateSuccess, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionUpdateSuccess.EXPECT().FindOne(ctxUpdateSuccess, bson.M{}).Return(mongoSingleResultUpdateSuccess),
		mongoSingleResultUpdateSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{
				Id: idUpdateSuccess,
			}))
			return nil
		}),
	)
	// success case calculate instance nil
	mongoCollectionCalculateNil := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultCalculateNil := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxCalculateNil := context.Background()
	instanceRepositoryCalculateNil := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaCalculateNil := []*instance.QuotaInstance{}
	instanceRepositoryCalculateNil.EXPECT().CalculateInstance(ctxCalculateNil).Return(instanceQuotaCalculateNil)
	resultInsertOneCalculateNil := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	var quotaVMCalculateNil = &QuotaVM{
		TotalCPU:         0,
		TotalMemory:      0,
		TotalStorage:     0,
		TotalCPUUsed:     0,
		TotalMemoryUsed:  0,
		TotalStorageUsed: 0,
		TotalVMUsed:      0,
	}
	mongoCollectionCalculateNil.EXPECT().InsertOne(ctxCalculateNil, quotaVMCalculateNil).Return(resultInsertOneCalculateNil, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionCalculateNil.EXPECT().FindOne(ctxCalculateNil, bson.M{}).Return(mongoSingleResultCalculateNil),
		mongoSingleResultCalculateNil.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{}))
			return nil
		}),
	)
	// update calculate instance nil
	mongoCollectionUpdateCalculateNil := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultUpdateCalculateNil := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxUpdateCalculateNil := context.Background()
	instanceRepositoryUpdateCalculateNil := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaUpdateCalculateNil := []*instance.QuotaInstance{}
	instanceQuotaUpdateCalculateNil = nil
	instanceRepositoryUpdateCalculateNil.EXPECT().CalculateInstance(ctxUpdateCalculateNil).Return(instanceQuotaUpdateCalculateNil)
	idUpdateCalculateNil := primitive.NewObjectID()
	resultUpdateCalculateNil := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
		UpsertedCount: 1,
		UpsertedID:    nil,
	}
	mongoCollectionUpdateCalculateNil.EXPECT().UpdateOne(ctxUpdateCalculateNil, bson.M{"_id": idUpdateCalculateNil}, gomock.Any()).Return(resultUpdateCalculateNil, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionUpdateCalculateNil.EXPECT().FindOne(ctxUpdateCalculateNil, bson.M{}).Return(mongoSingleResultUpdateCalculateNil),
		mongoSingleResultUpdateCalculateNil.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{
				Id:               idUpdateCalculateNil,
				TotalCPU:         10000,
				TotalMemory:      1000,
				TotalStorage:     1000000,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 1000,
				TotalVMUsed:      100,
			}))
			return nil
		}),
	)
	// success case find one with error
	mongoCollectionFindOneError := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultFindOneError := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxFindOneError := context.Background()
	instanceRepositoryFindOneError := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaFindOneError := []*instance.QuotaInstance{
		{
			Cpu:        100,
			Mem:        1000,
			Disk:       10000,
			TotalVM:    100,
			SelectType: instance.QuotaType,
		},
		{
			Cpu:        10,
			Mem:        10,
			Disk:       500,
			TotalVM:    10,
			SelectType: instance.UsageType,
		},
	}
	instanceRepositoryFindOneError.EXPECT().CalculateInstance(ctxFindOneError).Return(instanceQuotaFindOneError)
	resultInsertOneFindOneError := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	var quotaVMFindOneError = &QuotaVM{
		TotalCPU:         instanceQuotaFindOneError[0].Cpu,
		TotalMemory:      instanceQuotaFindOneError[0].Mem,
		TotalStorage:     instanceQuotaFindOneError[0].Disk,
		TotalCPUUsed:     instanceQuotaFindOneError[1].Cpu,
		TotalMemoryUsed:  instanceQuotaFindOneError[1].Mem,
		TotalStorageUsed: instanceQuotaFindOneError[1].Disk,
		TotalVMUsed:      instanceQuotaFindOneError[1].TotalVM,
	}
	mongoCollectionFindOneError.EXPECT().InsertOne(ctxFindOneError, quotaVMFindOneError).Return(resultInsertOneFindOneError, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionFindOneError.EXPECT().FindOne(ctxFindOneError, bson.M{}).Return(mongoSingleResultFindOneError),
		mongoSingleResultFindOneError.EXPECT().Decode(gomock.Any()).Return(errors.New("error")),
	)
	// error case insert one
	mongoCollectionInsertOneError := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultInsertOneError := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxInsertOneError := context.Background()
	instanceRepositoryInsertOneError := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaInsertOneError := []*instance.QuotaInstance{
		{
			Cpu:        100,
			Mem:        1000,
			Disk:       10000,
			TotalVM:    100,
			SelectType: instance.QuotaType,
		},
		{
			Cpu:        10,
			Mem:        10,
			Disk:       500,
			TotalVM:    10,
			SelectType: instance.UsageType,
		},
	}
	instanceRepositoryInsertOneError.EXPECT().CalculateInstance(ctxInsertOneError).Return(instanceQuotaInsertOneError)
	resultInsertOneInsertOneError := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	resultInsertOneInsertOneError = nil
	var quotaVMInsertOneError = &QuotaVM{
		TotalCPU:         instanceQuotaInsertOneError[0].Cpu,
		TotalMemory:      instanceQuotaInsertOneError[0].Mem,
		TotalStorage:     instanceQuotaInsertOneError[0].Disk,
		TotalCPUUsed:     instanceQuotaInsertOneError[1].Cpu,
		TotalMemoryUsed:  instanceQuotaInsertOneError[1].Mem,
		TotalStorageUsed: instanceQuotaInsertOneError[1].Disk,
		TotalVMUsed:      instanceQuotaInsertOneError[1].TotalVM,
	}
	mongoCollectionInsertOneError.EXPECT().InsertOne(ctxInsertOneError, quotaVMInsertOneError).Return(resultInsertOneInsertOneError, errors.New("error")).AnyTimes()
	gomock.InOrder(
		mongoCollectionInsertOneError.EXPECT().FindOne(ctxInsertOneError, bson.M{}).Return(mongoSingleResultInsertOneError),
		mongoSingleResultInsertOneError.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{}))
			return nil
		}),
	)
	// error case update one
	mongoCollectionUpdateOneError := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultUpdateOneError := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxUpdateOneError := context.Background()
	instanceRepositoryUpdateOneError := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuotaUpdateOneError := []*instance.QuotaInstance{
		{
			Cpu:        100,
			Mem:        1000,
			Disk:       10000,
			TotalVM:    100,
			SelectType: instance.QuotaType,
		},
		{
			Cpu:        10,
			Mem:        10,
			Disk:       500,
			TotalVM:    10,
			SelectType: instance.UsageType,
		},
	}
	instanceRepositoryUpdateOneError.EXPECT().CalculateInstance(ctxUpdateOneError).Return(instanceQuotaUpdateOneError)
	resultUpdateOneUpdateOneError := &pkgMongo.UpdateResult{
		MatchedCount:  1,
		ModifiedCount: 1,
		UpsertedCount: 0,
		UpsertedID:    nil,
	}
	resultUpdateOneUpdateOneError = nil
	idUpdateOneError := primitive.NewObjectID()
	mongoCollectionUpdateOneError.EXPECT().UpdateOne(ctxUpdateOneError, bson.M{"_id": idUpdateOneError}, gomock.Any()).Return(resultUpdateOneUpdateOneError, errors.New("error")).AnyTimes()
	gomock.InOrder(
		mongoCollectionUpdateOneError.EXPECT().FindOne(ctxUpdateOneError, bson.M{}).Return(mongoSingleResultUpdateOneError),
		mongoSingleResultUpdateOneError.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{
				Id: idUpdateOneError,
			}))
			return nil
		}),
	)
	type fields struct {
		Collection         mongo.Collection
		instanceRepository instance.VirtualInstanceRepository
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection:         mongoCollectionSuccess,
				instanceRepository: instanceRepositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			wantErr: false,
		},
		{
			name: "update success",
			fields: fields{
				Collection:         mongoCollectionUpdateSuccess,
				instanceRepository: instanceRepositoryUpdateSuccess,
			},
			args: args{
				ctx: ctxUpdateSuccess,
			},
			wantErr: false,
		},
		{
			name: "success calculate instance is nil",
			fields: fields{
				Collection:         mongoCollectionCalculateNil,
				instanceRepository: instanceRepositoryCalculateNil,
			},
			args: args{
				ctx: ctxCalculateNil,
			},
			wantErr: false,
		},
		{
			name: "update success calculate instance is nil",
			fields: fields{
				Collection:         mongoCollectionUpdateCalculateNil,
				instanceRepository: instanceRepositoryUpdateCalculateNil,
			},
			args: args{
				ctx: ctxUpdateCalculateNil,
			},
			wantErr: false,
		},
		{
			name: "find one error success",
			fields: fields{
				Collection:         mongoCollectionFindOneError,
				instanceRepository: instanceRepositoryFindOneError,
			},
			args: args{
				ctx: ctxFindOneError,
			},
			wantErr: false,
		},
		{
			name: "insert one error success",
			fields: fields{
				Collection:         mongoCollectionInsertOneError,
				instanceRepository: instanceRepositoryInsertOneError,
			},
			args: args{
				ctx: ctxInsertOneError,
			},
			wantErr: true,
		},
		{
			name: "update one error success",
			fields: fields{
				Collection:         mongoCollectionUpdateOneError,
				instanceRepository: instanceRepositoryUpdateOneError,
			},
			args: args{
				ctx: ctxUpdateOneError,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mongoContext{
				Collection:         tt.fields.Collection,
				instanceRepository: tt.fields.instanceRepository,
			}
			if err := m.UpdateReportQuotaVirtualMachine(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("UpdateReportQuotaVirtualMachine() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoContext_AddVirtualMachine(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultSuccess := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	instanceRepositorySuccess := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceQuota := &instance.QuotaInstance{
		Cpu:        100,
		Mem:        1000,
		Disk:       10000,
		TotalVM:    0,
		SelectType: instance.QuotaType,
	}
	instanceQuotaSuccess := []*instance.QuotaInstance{
		instanceQuota,
	}
	instanceRepositorySuccess.EXPECT().CalculateInstance(ctxSuccess).Return(instanceQuotaSuccess)
	instanceRepositorySuccess.EXPECT().AddInstance(ctxSuccess, instanceQuota, instance.QuotaType).Return(nil)
	resultSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	quotaVM := &QuotaVM{
		TotalCPU:         instanceQuota.Cpu,
		TotalMemory:      instanceQuota.Mem,
		TotalStorage:     instanceQuota.Disk,
		TotalCPUUsed:     0,
		TotalMemoryUsed:  0,
		TotalStorageUsed: 0,
		TotalVMUsed:      0,
	}
	mongoCollectionSuccess.EXPECT().InsertOne(ctxSuccess, quotaVM).Return(resultSuccess, nil).AnyTimes()
	gomock.InOrder(
		mongoCollectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(mongoSingleResultSuccess),
		mongoSingleResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{}))
			return nil
		}),
	)
	inQuotaSuccess := &virtual_machine.RequestQuotaVM{
		RequestMemory:  instanceQuota.Mem,
		RequestCpu:     instanceQuota.Cpu,
		RequestStorage: instanceQuota.Disk,
	}
	// add error
	mongoCollectionAddError := mockMongo.NewMockCollection(ctrl)
	ctxAddError := context.Background()
	instanceRepositoryAddError := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceRepositoryAddError.EXPECT().AddInstance(ctxAddError, instanceQuota, instance.QuotaType).Return(primitive.ErrInvalidHex)
	// update error
	mongoCollectionUpdateError := mockMongo.NewMockCollection(ctrl)
	ctxUpdateError := context.Background()
	instanceRepositoryUpdateError := instanceMock.NewMockVirtualInstanceRepository(ctrl)
	instanceRepositoryUpdateError.EXPECT().CalculateInstance(ctxUpdateError).Return(instanceQuotaSuccess)
	instanceRepositoryUpdateError.EXPECT().AddInstance(ctxUpdateError, instanceQuota, instance.QuotaType).Return(nil)
	resultInsertError := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	resultInsertError = nil
	mongoCollectionUpdateError.EXPECT().InsertOne(ctxUpdateError, quotaVM).Return(resultInsertError, errors.New("error")).AnyTimes()
	gomock.InOrder(
		mongoCollectionUpdateError.EXPECT().FindOne(ctxUpdateError, bson.M{}).Return(mongoSingleResultSuccess),
		mongoSingleResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(&QuotaVM{}))
			return nil
		}),
	)
	type fields struct {
		Collection         mongo.Collection
		instanceRepository instance.VirtualInstanceRepository
	}
	type args struct {
		ctx context.Context
		in  *virtual_machine.RequestQuotaVM
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection:         mongoCollectionSuccess,
				instanceRepository: instanceRepositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inQuotaSuccess,
			},
			wantErr: false,
		},
		{
			name: "add error",
			fields: fields{
				Collection:         mongoCollectionAddError,
				instanceRepository: instanceRepositoryAddError,
			},
			args: args{
				ctx: ctxAddError,
				in:  inQuotaSuccess,
			},
			wantErr: true,
		},
		{
			name: "update error",
			fields: fields{
				Collection:         mongoCollectionUpdateError,
				instanceRepository: instanceRepositoryUpdateError,
			},
			args: args{
				ctx: ctxUpdateError,
				in:  inQuotaSuccess,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mongoContext{
				Collection:         tt.fields.Collection,
				instanceRepository: tt.fields.instanceRepository,
			}
			if err := m.AddVirtualMachine(tt.args.ctx, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("AddVirtualMachine() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_mongoContext_GetReportQuotaVirtualMachine(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultSuccess := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	vmQuota := &QuotaVM{
		TotalCPU:         1,
		TotalMemory:      1,
		TotalStorage:     1,
		TotalCPUUsed:     1,
		TotalMemoryUsed:  1,
		TotalStorageUsed: 1,
		TotalVMUsed:      1,
	}
	gomock.InOrder(
		mongoCollectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(mongoSingleResultSuccess),
		mongoSingleResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(v interface{}) error {
			reflect.ValueOf(v).Elem().Set(reflect.ValueOf(vmQuota))
			return nil
		}),
	)
	// error
	mongoCollectionError := mockMongo.NewMockCollection(ctrl)
	mongoSingleResultError := mockMongo.NewMockSingleResultDecoder(ctrl)
	ctxError := context.Background()
	gomock.InOrder(
		mongoCollectionError.EXPECT().FindOne(ctxError, bson.M{}).Return(mongoSingleResultError),
		mongoSingleResultError.EXPECT().Decode(gomock.Any()).Return(errors.New("error")),
	)
	type fields struct {
		Collection         mongo.Collection
		instanceRepository instance.VirtualInstanceRepository
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *QuotaVM
	}{
		{
			name: "success",
			fields: fields{
				Collection:         mongoCollectionSuccess,
				instanceRepository: nil,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want: vmQuota,
		},
		{
			name: "error",
			fields: fields{
				Collection:         mongoCollectionError,
				instanceRepository: nil,
			},
			args: args{
				ctx: ctxError,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &mongoContext{
				Collection:         tt.fields.Collection,
				instanceRepository: tt.fields.instanceRepository,
			}
			if got := m.GetReportQuotaVirtualMachine(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetReportQuotaVirtualMachine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mongoCollectionSuccess := mockMongo.NewMockCollection(ctrl)
	instanceRepositorySuccess := instanceMock.NewMockVirtualInstanceRepository(ctrl)

	type args struct {
		collection         mongo.Collection
		instanceRepository instance.VirtualInstanceRepository
	}
	tests := []struct {
		name string
		args args
		want VirtualRepository
	}{
		{
			name: "success",
			args: args{
				collection:         nil,
				instanceRepository: nil,
			},
			want: &mongoContext{
				Collection:         nil,
				instanceRepository: nil,
			},
		},
		{
			name: "success with instance repository",
			args: args{
				collection:         mongoCollectionSuccess,
				instanceRepository: instanceRepositorySuccess,
			},
			want: &mongoContext{
				Collection:         mongoCollectionSuccess,
				instanceRepository: instanceRepositorySuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewRepository(tt.args.collection, tt.args.instanceRepository); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
