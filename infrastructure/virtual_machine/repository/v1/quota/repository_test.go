package quota

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestQuotaVM_GetMemoryPercentUsed(t *testing.T) {
	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Memory Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test Memory Percent Zero of TotalMemory",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test Memory Percent Zero of TotalMemoryUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetMemoryPercentUsed(); got != tt.want {
				t.Errorf("GetMemoryPercentUsed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotaVM_GetCpuPercentUsed(t *testing.T) {

	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test CPU Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test CPU Percent Zero of TotalCPU",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test CPU Percent Zero of TotalCPUUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test CPU Percent Zero of TotalCPU is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 100,
				TotalVMUsed:      100,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetCpuPercentUsed(); got != tt.want {
				t.Errorf("GetCpuPercentUsed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotaVM_GetStoragePercentUsed(t *testing.T) {

	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Storage Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test Storage Percent Zero of TotalStorage",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test Storage Percent Zero of TotalStorageUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test Storage Percent Zero of TotalStorage is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 100,
				TotalVMUsed:      100,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetStoragePercentUsed(); got != tt.want {
				t.Errorf("GetStoragePercentUsed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotaVM_GetMemoryPercentFree(t *testing.T) {
	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Memory Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test Memory Percent Zero of TotalMemory",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test Memory Percent Zero of TotalMemoryUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 100,
		},
		{
			name: "Test Memory Percent Zero of TotalMemory is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 100,
				TotalVMUsed:      100,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetMemoryPercentFree(); got != tt.want {
				t.Errorf("GetMemoryPercentFree() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotaVM_GetCpuPercentFree(t *testing.T) {
	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test CPU Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test CPU Percent Zero of TotalCPU",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test CPU Percent Zero of TotalCPUUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 100,
		},
		{
			name: "Test CPU Percent Zero of TotalCPU is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 100,
				TotalVMUsed:      100,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetCpuPercentFree(); got != tt.want {
				t.Errorf("GetCpuPercentFree() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestQuotaVM_GetStoragePercentFree(t *testing.T) {
	type fields struct {
		Id               primitive.ObjectID
		TotalCPU         int64
		TotalMemory      int64
		TotalStorage     int64
		TotalCPUUsed     int64
		TotalMemoryUsed  int64
		TotalStorageUsed int64
		TotalVMUsed      int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Storage Percent",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     50,
				TotalMemoryUsed:  50,
				TotalStorageUsed: 50,
				TotalVMUsed:      50,
			},
			want: 50,
		},
		{
			name: "Test Storage Percent Zero of TotalStorage",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 0,
		},
		{
			name: "Test Storage Percent Zero of TotalStorageUsed is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         100,
				TotalMemory:      100,
				TotalStorage:     100,
				TotalCPUUsed:     0,
				TotalMemoryUsed:  0,
				TotalStorageUsed: 0,
				TotalVMUsed:      0,
			},
			want: 100,
		},
		{
			name: "Test Storage Percent Zero of TotalStorage is Zero",
			fields: fields{
				Id:               primitive.NewObjectID(),
				TotalCPU:         0,
				TotalMemory:      0,
				TotalStorage:     0,
				TotalCPUUsed:     100,
				TotalMemoryUsed:  100,
				TotalStorageUsed: 100,
				TotalVMUsed:      100,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := QuotaVM{
				Id:               tt.fields.Id,
				TotalCPU:         tt.fields.TotalCPU,
				TotalMemory:      tt.fields.TotalMemory,
				TotalStorage:     tt.fields.TotalStorage,
				TotalCPUUsed:     tt.fields.TotalCPUUsed,
				TotalMemoryUsed:  tt.fields.TotalMemoryUsed,
				TotalStorageUsed: tt.fields.TotalStorageUsed,
				TotalVMUsed:      tt.fields.TotalVMUsed,
			}
			if got := v.GetStoragePercentFree(); got != tt.want {
				t.Errorf("GetStoragePercentFree() = %v, want %v", got, tt.want)
			}
		})
	}
}
