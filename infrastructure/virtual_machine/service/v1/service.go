package v1

import (
	"context"
	"errors"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	repository "moi-quota/infrastructure/virtual_machine/repository/v1/quota"
	"moi-quota/pkg/api/infrastructure/v1/virtual_machine"
	protocol "moi-quota/pkg/protocol/v1"
)

type InternalServiceVirtualMachine interface {
	ValidateRequestQuotaVM(in interface{}) error
}
type internalServiceContext struct {
}

const StatusAddQuotaVMError = "ERROR_ADD_QUOTA_VM"
const StatusAddCPUError = "ERROR_ADD_CPU"
const StatusAddMemoryError = "ERROR_ADD_MEMORY"
const StatusAddQuotaVMSuccess = "SUCCESS_ADD_QUOTA_VM"
const StatusGetQuotaVMSuccess = "SUCCESS_GET_QUOTA_VM"

func (i internalServiceContext) ValidateRequestQuotaVM(in interface{}) error {
	switch in.(type) {
	case *virtual_machine.RequestQuotaVM:
		var errorResponse = status.New(codes.InvalidArgument, StatusAddQuotaVMError)
		if in.(*virtual_machine.RequestQuotaVM).GetRequestCpu() < 0 {
			errorResponse, _ = errorResponse.WithDetails(&virtual_machine.ErrRequestQuotaVM{
				Code:    StatusAddCPUError,
				Message: "Request CPU must be greater than 0",
			})
			return errorResponse.Err()
		}
		if in.(*virtual_machine.RequestQuotaVM).GetRequestMemory() < 0 {
			errorResponse, _ = errorResponse.WithDetails(&virtual_machine.ErrRequestQuotaVM{
				Code:    StatusAddMemoryError,
				Message: "Request Memory must be greater than 0",
			})
			return errorResponse.Err()
		}
		if in.(*virtual_machine.RequestQuotaVM).GetRequestStorage() < 0 {
			errorResponse, _ = errorResponse.WithDetails(&virtual_machine.ErrRequestQuotaVM{
				Code:    StatusAddMemoryError,
				Message: "Request Storage must be greater than 0",
			})
			return errorResponse.Err()
		}
		return nil
	default:
		return errors.New("invalid type")
	}
}

type virtualMachineContext struct {
	virtual_machine.UnimplementedVirtualMachineServiceServer
	repositoryVirtualMachine repository.VirtualRepository
	internalService          InternalServiceVirtualMachine
}

func (c virtualMachineContext) RequestQuotaVirtualMachine(ctx context.Context, in *virtual_machine.RequestQuotaVM) (*virtual_machine.ResponseQuotaVM, error) {
	if err := c.internalService.ValidateRequestQuotaVM(in); err != nil {
		return nil, err
	}
	err := c.repositoryVirtualMachine.AddVirtualMachine(ctx, in)
	if err != nil {
		return nil, err
	}
	quota := c.repositoryVirtualMachine.GetReportQuotaVirtualMachine(ctx)
	if quota == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Quota add failed")
	}

	return &virtual_machine.ResponseQuotaVM{
		MemoryQuota:        quota.TotalMemory,
		CpuQuota:           quota.TotalCPU,
		StorageQuota:       quota.TotalStorage,
		MemoryUsage:        quota.TotalMemoryUsed,
		CpuUsed:            quota.TotalCPUUsed,
		StorageUsed:        quota.TotalStorageUsed,
		MemoryPercentUsed:  quota.GetMemoryPercentUsed(),
		CpuUsagePercent:    quota.GetCpuPercentUsed(),
		StorageUsedPercent: quota.GetStoragePercentUsed(),
		MemoryFreePercent:  quota.GetMemoryPercentFree(),
		CpuFreePercent:     quota.GetCpuPercentFree(),
		StorageFreePercent: quota.GetStoragePercentFree(),
		Message:            StatusAddQuotaVMSuccess,
	}, nil
}

func (c virtualMachineContext) GetQuotaVirtualMachine(ctx context.Context, in *empty.Empty) (*virtual_machine.ResponseQuotaVM, error) {
	quota := c.repositoryVirtualMachine.GetReportQuotaVirtualMachine(ctx)
	if quota == nil {
		return nil, status.Errorf(codes.NotFound, "Quota not found")
	}
	return &virtual_machine.ResponseQuotaVM{
		MemoryQuota:        quota.TotalMemory,
		CpuQuota:           quota.TotalCPU,
		StorageQuota:       quota.TotalStorage,
		MemoryUsage:        quota.TotalMemoryUsed,
		CpuUsed:            quota.TotalCPUUsed,
		StorageUsed:        quota.TotalStorageUsed,
		MemoryPercentUsed:  quota.GetMemoryPercentUsed(),
		CpuUsagePercent:    quota.GetCpuPercentUsed(),
		StorageUsedPercent: quota.GetStoragePercentUsed(),
		MemoryFreePercent:  quota.GetMemoryPercentFree(),
		CpuFreePercent:     quota.GetCpuPercentFree(),
		StorageFreePercent: quota.GetStoragePercentFree(),
		Message:            StatusGetQuotaVMSuccess,
	}, nil
}

type virtualMachineService struct {
	service virtual_machine.VirtualMachineServiceServer
	ctx     context.Context
}

func (v virtualMachineService) TcpListening(server *grpc.Server) {
	virtual_machine.RegisterVirtualMachineServiceServer(server, v.service)
}

func (v virtualMachineService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return virtual_machine.RegisterVirtualMachineServiceHandler(v.ctx, mux, conn)
}

func initVirtualMachineServiceServer(internalService InternalServiceVirtualMachine, repositoryVM repository.VirtualRepository) virtual_machine.VirtualMachineServiceServer {
	return &virtualMachineContext{
		repositoryVirtualMachine: repositoryVM,
		internalService:          internalService,
	}
}

func NewProtocolService(ctx context.Context, repositoryVM repository.VirtualRepository) protocol.Service {
	return &virtualMachineService{
		service: initVirtualMachineServiceServer(
			&internalServiceContext{},
			repositoryVM,
		),
		ctx: ctx,
	}
}
