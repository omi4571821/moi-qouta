package v1

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc"
	"moi-quota/infrastructure/virtual_machine/repository/v1/quota"
	repositoryMock "moi-quota/mock/infrastructure/virtual_machine/repository/v1/quota"
	serviceMock "moi-quota/mock/infrastructure/virtual_machine/service/v1"
	"moi-quota/pkg/api/infrastructure/v1/virtual_machine"
	v1 "moi-quota/pkg/protocol/v1"
	"reflect"
	"testing"
)

func Test_internalServiceContext_ValidateRequestQuotaVM(t *testing.T) {
	// success case
	inSuccess := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	// fail case
	inFail := &virtual_machine.RequestQuotaVM{
		RequestMemory:  -1,
		RequestCpu:     -1,
		RequestStorage: -1,
	}
	// fail case negative memory
	inFailNegativeMemory := &virtual_machine.RequestQuotaVM{
		RequestMemory:  -1,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	// fail case negative cpu
	inFailNegativeCpu := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     -1,
		RequestStorage: 1000,
	}
	// fail case negative storage
	inFailNegativeStorage := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     10,
		RequestStorage: -1,
	}
	// fail unknown type
	inFailUnknownType := map[string]interface{}{
		"RequestMemory":  100,
		"RequestCpu":     10,
		"RequestStorage": 100,
	}
	type args struct {
		in interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "success",
			args:    args{in: inSuccess},
			wantErr: false,
		},
		{
			name:    "fail",
			args:    args{in: inFail},
			wantErr: true,
		},
		{
			name:    "fail negative memory",
			args:    args{in: inFailNegativeMemory},
			wantErr: true,
		},
		{
			name:    "fail negative cpu",
			args:    args{in: inFailNegativeCpu},
			wantErr: true,
		},
		{
			name:    "fail negative storage",
			args:    args{in: inFailNegativeStorage},
			wantErr: true,
		},
		{
			name:    "fail unknown type",
			args:    args{in: inFailUnknownType},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := internalServiceContext{}
			if err := i.ValidateRequestQuotaVM(tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("ValidateRequestQuotaVM() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_virtualMachineContext_RequestQuotaVirtualMachine(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	inSuccess := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	ctxSuccess := context.Background()
	vmQuota := &quota.QuotaVM{
		Id:               primitive.NewObjectID(),
		TotalCPU:         1000,
		TotalMemory:      100000,
		TotalStorage:     1000000,
		TotalCPUUsed:     100,
		TotalMemoryUsed:  1000,
		TotalStorageUsed: 10000,
		TotalVMUsed:      60,
	}

	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	serviceVirtualMachineSuccess := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceVirtualMachineSuccess.EXPECT().ValidateRequestQuotaVM(inSuccess).Return(nil)
	repositoryVirtualMachineSuccess.EXPECT().AddVirtualMachine(ctxSuccess, inSuccess).Return(nil)
	repositoryVirtualMachineSuccess.EXPECT().GetReportQuotaVirtualMachine(ctxSuccess).Return(vmQuota)
	responseSuccess := &virtual_machine.ResponseQuotaVM{
		MemoryQuota:        vmQuota.TotalMemory,
		CpuQuota:           vmQuota.TotalCPU,
		StorageQuota:       vmQuota.TotalStorage,
		MemoryUsage:        vmQuota.TotalMemoryUsed,
		CpuUsed:            vmQuota.TotalCPUUsed,
		StorageUsed:        vmQuota.TotalStorageUsed,
		MemoryPercentUsed:  vmQuota.GetMemoryPercentUsed(),
		CpuUsagePercent:    vmQuota.GetCpuPercentUsed(),
		StorageUsedPercent: vmQuota.GetStoragePercentUsed(),
		MemoryFreePercent:  vmQuota.GetMemoryPercentFree(),
		CpuFreePercent:     vmQuota.GetCpuPercentFree(),
		StorageFreePercent: vmQuota.GetStoragePercentFree(),
		Message:            StatusAddQuotaVMSuccess,
	}
	// fail case validate
	inFailValidate := &virtual_machine.RequestQuotaVM{
		RequestMemory:  -100,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	ctxFailValidate := context.Background()
	serviceVirtualMachineFailValidate := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceVirtualMachineFailValidate.EXPECT().ValidateRequestQuotaVM(inFailValidate).Return(errors.New("error"))
	repositoryVirtualMachineFailValidate := repositoryMock.NewMockVirtualRepository(ctrl)
	responseFailValidate := &virtual_machine.ResponseQuotaVM{}
	responseFailValidate = nil
	// fail case add
	inFailAdd := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	ctxFailAdd := context.Background()
	serviceVirtualMachineFailAdd := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceVirtualMachineFailAdd.EXPECT().ValidateRequestQuotaVM(inFailAdd).Return(nil)
	repositoryVirtualMachineFailAdd := repositoryMock.NewMockVirtualRepository(ctrl)
	repositoryVirtualMachineFailAdd.EXPECT().AddVirtualMachine(ctxFailAdd, inFailAdd).Return(errors.New("error"))
	responseFailAdd := &virtual_machine.ResponseQuotaVM{}
	responseFailAdd = nil
	// fail case get report nil
	inFailGetReport := &virtual_machine.RequestQuotaVM{
		RequestMemory:  100,
		RequestCpu:     10,
		RequestStorage: 1000,
	}
	ctxFailGetReport := context.Background()
	serviceVirtualMachineFailGetReport := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceVirtualMachineFailGetReport.EXPECT().ValidateRequestQuotaVM(inFailGetReport).Return(nil)
	repositoryVirtualMachineFailGetReport := repositoryMock.NewMockVirtualRepository(ctrl)
	repositoryVirtualMachineFailGetReport.EXPECT().AddVirtualMachine(ctxFailGetReport, inFailGetReport).Return(nil)
	repositoryVirtualMachineFailGetReport.EXPECT().GetReportQuotaVirtualMachine(ctxFailGetReport).Return(nil)
	responseFailGetReport := &virtual_machine.ResponseQuotaVM{}
	responseFailGetReport = nil

	type fields struct {
		UnimplementedVirtualMachineServiceServer virtual_machine.UnimplementedVirtualMachineServiceServer
		repositoryVirtualMachine                 quota.VirtualRepository
		internalService                          InternalServiceVirtualMachine
	}
	type args struct {
		ctx context.Context
		in  *virtual_machine.RequestQuotaVM
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *virtual_machine.ResponseQuotaVM
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineSuccess,
				internalService:                          serviceVirtualMachineSuccess,
			},
			args:    args{ctx: ctxSuccess, in: inSuccess},
			want:    responseSuccess,
			wantErr: false,
		},
		{
			name: "fail validate",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineFailValidate,
				internalService:                          serviceVirtualMachineFailValidate,
			},
			args:    args{ctx: ctxFailValidate, in: inFailValidate},
			want:    responseFailValidate,
			wantErr: true,
		},
		{
			name: "fail add",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineFailAdd,
				internalService:                          serviceVirtualMachineFailAdd,
			},
			args:    args{ctx: ctxFailAdd, in: inFailAdd},
			want:    responseFailAdd,
			wantErr: true,
		},
		{
			name: "fail get report",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineFailGetReport,
				internalService:                          serviceVirtualMachineFailGetReport,
			},
			args:    args{ctx: ctxFailGetReport, in: inFailGetReport},
			want:    responseFailGetReport,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := virtualMachineContext{
				UnimplementedVirtualMachineServiceServer: tt.fields.UnimplementedVirtualMachineServiceServer,
				repositoryVirtualMachine:                 tt.fields.repositoryVirtualMachine,
				internalService:                          tt.fields.internalService,
			}
			got, err := c.RequestQuotaVirtualMachine(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("RequestQuotaVirtualMachine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RequestQuotaVirtualMachine() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_virtualMachineContext_GetQuotaVirtualMachine(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	ctxSuccess := context.Background()
	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	vmQuota := &quota.QuotaVM{
		Id:               primitive.NewObjectID(),
		TotalCPU:         100,
		TotalMemory:      100,
		TotalStorage:     1000,
		TotalCPUUsed:     100,
		TotalMemoryUsed:  10,
		TotalStorageUsed: 10,
		TotalVMUsed:      10,
	}
	repositoryVirtualMachineSuccess.EXPECT().GetReportQuotaVirtualMachine(ctxSuccess).Return(vmQuota)
	responseSuccess := &virtual_machine.ResponseQuotaVM{
		MemoryQuota:        vmQuota.TotalMemory,
		CpuQuota:           vmQuota.TotalCPU,
		StorageQuota:       vmQuota.TotalStorage,
		MemoryUsage:        vmQuota.TotalMemoryUsed,
		CpuUsed:            vmQuota.TotalCPUUsed,
		StorageUsed:        vmQuota.TotalStorageUsed,
		MemoryPercentUsed:  vmQuota.GetMemoryPercentUsed(),
		CpuUsagePercent:    vmQuota.GetCpuPercentUsed(),
		StorageUsedPercent: vmQuota.GetStoragePercentUsed(),
		MemoryFreePercent:  vmQuota.GetMemoryPercentFree(),
		CpuFreePercent:     vmQuota.GetCpuPercentFree(),
		StorageFreePercent: vmQuota.GetStoragePercentFree(),
		Message:            StatusGetQuotaVMSuccess,
	}
	// fail case get report
	ctxFailGetReport := context.Background()
	repositoryVirtualMachineFailGetReport := repositoryMock.NewMockVirtualRepository(ctrl)
	repositoryVirtualMachineFailGetReport.EXPECT().GetReportQuotaVirtualMachine(ctxFailGetReport).Return(nil)
	responseFailGetReport := &virtual_machine.ResponseQuotaVM{}
	responseFailGetReport = nil

	type fields struct {
		UnimplementedVirtualMachineServiceServer virtual_machine.UnimplementedVirtualMachineServiceServer
		repositoryVirtualMachine                 quota.VirtualRepository
		internalService                          InternalServiceVirtualMachine
	}
	type args struct {
		ctx context.Context
		in  *empty.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *virtual_machine.ResponseQuotaVM
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineSuccess,
			},
			args: args{ctx: ctxSuccess, in: &empty.Empty{}},
			want: responseSuccess,
		},
		{
			name: "fail get report",
			fields: fields{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineFailGetReport,
			},
			args:    args{ctx: ctxFailGetReport, in: &empty.Empty{}},
			want:    responseFailGetReport,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := virtualMachineContext{
				UnimplementedVirtualMachineServiceServer: tt.fields.UnimplementedVirtualMachineServiceServer,
				repositoryVirtualMachine:                 tt.fields.repositoryVirtualMachine,
				internalService:                          tt.fields.internalService,
			}
			got, err := c.GetQuotaVirtualMachine(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetQuotaVirtualMachine() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetQuotaVirtualMachine() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_virtualMachineService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	ctxSuccess := context.Background()
	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	serviceVirtualMachineSuccess := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceSuccess := &virtualMachineContext{
		repositoryVirtualMachine: repositoryVirtualMachineSuccess,
		internalService:          serviceVirtualMachineSuccess,
	}
	type fields struct {
		service virtual_machine.VirtualMachineServiceServer
		ctx     context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				service: serviceSuccess,
				ctx:     ctxSuccess,
			},
			args: args{server: grpc.NewServer()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := virtualMachineService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			v.TcpListening(tt.args.server)
		})
	}
}

func Test_virtualMachineService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	ctxSuccess := context.Background()
	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	serviceVirtualMachineSuccess := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	serviceSuccess := &virtualMachineContext{
		repositoryVirtualMachine: repositoryVirtualMachineSuccess,
		internalService:          serviceVirtualMachineSuccess,
	}
	type fields struct {
		service virtual_machine.VirtualMachineServiceServer
		ctx     context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				service: serviceSuccess,
				ctx:     ctxSuccess,
			},
			args:    args{mux: runtime.NewServeMux(), conn: nil},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := virtualMachineService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			if err := v.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initVirtualMachineServiceServer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	serviceVirtualMachineSuccess := serviceMock.NewMockInternalServiceVirtualMachine(ctrl)
	type args struct {
		internalService InternalServiceVirtualMachine
		repositoryVM    quota.VirtualRepository
	}
	tests := []struct {
		name string
		args args
		want virtual_machine.VirtualMachineServiceServer
	}{
		{
			name: "success",
			args: args{
				internalService: serviceVirtualMachineSuccess,
				repositoryVM:    repositoryVirtualMachineSuccess,
			},
			want: &virtualMachineContext{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 repositoryVirtualMachineSuccess,
				internalService:                          serviceVirtualMachineSuccess,
			},
		},
		{
			name: "success nil",
			args: args{
				internalService: nil,
				repositoryVM:    nil,
			},
			want: &virtualMachineContext{
				UnimplementedVirtualMachineServiceServer: virtual_machine.UnimplementedVirtualMachineServiceServer{},
				repositoryVirtualMachine:                 nil,
				internalService:                          nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initVirtualMachineServiceServer(tt.args.internalService, tt.args.repositoryVM); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initVirtualMachineServiceServer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewProtocolService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositoryVirtualMachineSuccess := repositoryMock.NewMockVirtualRepository(ctrl)
	ctxSuccess := context.Background()
	serviceSuccess := &virtualMachineContext{
		repositoryVirtualMachine: repositoryVirtualMachineSuccess,
		internalService:          &internalServiceContext{},
	}
	type args struct {
		ctx          context.Context
		repositoryVM quota.VirtualRepository
	}
	tests := []struct {
		name string
		args args
		want v1.Service
	}{
		{
			name: "success",
			args: args{
				ctx:          ctxSuccess,
				repositoryVM: repositoryVirtualMachineSuccess,
			},
			want: &virtualMachineService{
				service: serviceSuccess,
				ctx:     ctxSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewProtocolService(tt.args.ctx, tt.args.repositoryVM); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProtocolService() = %v, want %v", got, tt.want)
			}
		})
	}
}
