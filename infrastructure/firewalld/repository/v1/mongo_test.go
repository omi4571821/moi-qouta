package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	driverMongoMock "moi-quota/mock/driver/mongo"
	"moi-quota/pkg/api/infrastructure/v1/firewalld"
	"reflect"
	"testing"
)

func Test_mongoContext_GetFirewall(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	collectionSuccess := driverMongoMock.NewMockCollection(ctrl)
	singResultSuccess := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	wantSuccess := &FirewalldQuota{
		Id:    primitive.NewObjectID(),
		Quota: 100,
		Usage: 50,
	}
	gomock.InOrder(
		collectionSuccess.EXPECT().FindOne(ctxSuccess, gomock.Any()).Return(singResultSuccess),
		singResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(wantSuccess))
			return nil
		}),
	)
	// empty case
	collectionEmpty := driverMongoMock.NewMockCollection(ctrl)
	singResultEmpty := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxEmpty := context.Background()
	wantEmpty := &FirewalldQuota{
		Quota: 0,
		Usage: 0,
	}
	gomock.InOrder(
		collectionEmpty.EXPECT().FindOne(ctxEmpty, gomock.Any()).Return(singResultEmpty),
		singResultEmpty.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *FirewalldQuota
		wantErr bool
	}{
		{
			name: "Test Get Firewall Success",
			fields: fields{
				Collection: collectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "Test Get Firewall Empty",
			fields: fields{
				Collection: collectionEmpty,
			},
			args: args{
				ctx: ctxEmpty,
			},
			want:    wantEmpty,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetFirewall(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFirewall() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFirewall() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoContext_CreateQuotaFirewall(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	collectionSuccess := driverMongoMock.NewMockCollection(ctrl)
	singResultSuccess := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	inSuccess := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	quota := &FirewalldQuota{
		Quota: 100,
	}
	insertResultSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	collectionSuccess.EXPECT().InsertOne(ctxSuccess, quota).Return(insertResultSuccess, nil)
	gomock.InOrder(
		collectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(singResultSuccess),
		singResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// update case
	collectionUpdate := driverMongoMock.NewMockCollection(ctrl)
	singResultUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxUpdate := context.Background()
	inUpdate := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	idUpdate := primitive.NewObjectID()
	quotaUpdate := &FirewalldQuota{
		Id:    idUpdate,
		Quota: 10,
	}
	updateResultUpdate := &pkgMongo.UpdateResult{
		ModifiedCount: 1,
	}
	collectionUpdate.EXPECT().UpdateOne(ctxUpdate, bson.M{"_id": idUpdate}, gomock.Any()).Return(updateResultUpdate, nil)
	gomock.InOrder(
		collectionUpdate.EXPECT().FindOne(ctxUpdate, bson.M{}).Return(singResultUpdate),
		singResultUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaUpdate))
			return nil
		}),
	)
	// error case insert
	collectionErrorInsert := driverMongoMock.NewMockCollection(ctrl)
	singResultErrorInsert := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxErrorInsert := context.Background()
	inErrorInsert := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	quotaErrorInsert := &FirewalldQuota{
		Quota: 100,
	}
	collectionErrorInsert.EXPECT().InsertOne(ctxErrorInsert, quotaErrorInsert).Return(nil, pkgMongo.ErrWrongClient)
	gomock.InOrder(
		collectionErrorInsert.EXPECT().FindOne(ctxErrorInsert, bson.M{}).Return(singResultErrorInsert),
		singResultErrorInsert.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// error case update
	collectionErrorUpdate := driverMongoMock.NewMockCollection(ctrl)
	singResultErrorUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxErrorUpdate := context.Background()
	inErrorUpdate := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	idErrorUpdate := primitive.NewObjectID()
	quotaErrorUpdate := &FirewalldQuota{
		Id:    idErrorUpdate,
		Quota: 10,
	}
	collectionErrorUpdate.EXPECT().UpdateOne(ctxErrorUpdate, bson.M{"_id": idErrorUpdate}, gomock.Any()).Return(nil, pkgMongo.ErrWrongClient)
	gomock.InOrder(
		collectionErrorUpdate.EXPECT().FindOne(ctxErrorUpdate, bson.M{}).Return(singResultErrorUpdate),
		singResultErrorUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaErrorUpdate))
			return nil
		}),
	)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
		in  *firewalld.RequestFirewall
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "Test Create Firewall Success",
			fields: fields{
				Collection: collectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			wantErr: false,
		},
		{
			name: "Test Update Firewall Success",
			fields: fields{
				Collection: collectionUpdate,
			},
			args: args{
				ctx: ctxUpdate,
				in:  inUpdate,
			},
			wantErr: false,
		},
		{
			name: "Test Create Firewall Error Insert",
			fields: fields{
				Collection: collectionErrorInsert,
			},
			args: args{
				ctx: ctxErrorInsert,
				in:  inErrorInsert,
			},
			wantErr: true,
		},
		{
			name: "Test Create Firewall Error Update",
			fields: fields{
				Collection: collectionErrorUpdate,
			},
			args: args{
				ctx: ctxErrorUpdate,
				in:  inErrorUpdate,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if err := m.CreateQuotaFirewall(tt.args.ctx, tt.args.in); (err != nil) != tt.wantErr {
				t.Errorf("CreateQuotaFirewall() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMongoRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	collection := driverMongoMock.NewMockCollection(ctrl)
	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want RepositoryFirewalld
	}{
		{
			name: "Test New Mongo Repository",
			args: args{
				collection: collection,
			},
			want: &mongoContext{
				Collection: collection,
			},
		},
		{
			name: "Test New Mongo Repository Nil",
			args: args{
				collection: nil,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
