package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"moi-quota/pkg/api/infrastructure/v1/firewalld"
)

type FirewalldQuota struct {
	Id    primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Quota int64              `json:"quota" bson:"quota"`
	Usage int64              `json:"usage" bson:"usage"`
}

func (q FirewalldQuota) GetUsagePercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return float32(q.Usage) / float32(q.Quota) * 100
}

func (q FirewalldQuota) GetFreePercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return 100 - q.GetUsagePercent()
}

type RepositoryFirewalld interface {
	GetFirewall(ctx context.Context) (*FirewalldQuota, error)
	CreateQuotaFirewall(ctx context.Context, in *firewalld.RequestFirewall) error
}
