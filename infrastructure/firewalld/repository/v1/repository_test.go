package v1

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestFirewalldQuota_GetFreePercent(t *testing.T) {
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Usage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Free Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Usage: 50,
			},
			want: 50,
		},
		{
			name: "Test Free Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Usage: 0,
			},
			want: 0,
		},
		{
			name: "Test Free Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Usage: 50,
			},
			want: 0,
		},
		{
			name: "Test Free Percent Usage Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Usage: 0,
			},
			want: 100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := FirewalldQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Usage: tt.fields.Usage,
			}
			if got := q.GetFreePercent(); got != tt.want {
				t.Errorf("GetFreePercent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFirewalldQuota_GetUsagePercent(t *testing.T) {
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Usage int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Usage Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Usage: 50,
			},
			want: 50,
		},
		{
			name: "Test Usage Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Usage: 0,
			},
			want: 0,
		},
		{
			name: "Test Usage Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Usage: 50,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := FirewalldQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Usage: tt.fields.Usage,
			}
			if got := q.GetUsagePercent(); got != tt.want {
				t.Errorf("GetUsagePercent() = %v, want %v", got, tt.want)
			}
		})
	}
}
