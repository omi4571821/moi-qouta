package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"moi-quota/driver/mongo"
	"moi-quota/pkg/api/infrastructure/v1/firewalld"
)

const CollectionName = "quota_firewall"

type mongoContext struct {
	mongo.Collection
}

func (m mongoContext) GetFirewall(ctx context.Context) (*FirewalldQuota, error) {
	var currentQuota *FirewalldQuota
	cursor := m.FindOne(ctx, bson.M{})
	if err := cursor.Decode(&currentQuota); err != nil {
		return &FirewalldQuota{
			Quota: 0,
			Usage: 0,
		}, err
	}
	return currentQuota, nil
}

func (m mongoContext) CreateQuotaFirewall(ctx context.Context, in *firewalld.RequestFirewall) error {
	quota := &FirewalldQuota{
		Quota: 0,
	}
	cursor := m.FindOne(ctx, bson.M{})
	err := cursor.Decode(&quota)
	quota.Quota += in.RequestFirewallQuota
	if quota.Id.IsZero() {
		_, err = m.InsertOne(ctx, quota)
		return err
	}
	_, err = m.UpdateOne(ctx, bson.M{"_id": quota.Id}, bson.M{"$set": quota})
	return err

}

func NewMongoRepository(collection mongo.Collection) RepositoryFirewalld {
	if collection == nil {
		return nil
	}
	return &mongoContext{collection}

}
