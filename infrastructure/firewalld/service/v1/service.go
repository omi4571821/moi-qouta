package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	repository "moi-quota/infrastructure/firewalld/repository/v1"
	"moi-quota/pkg/api/infrastructure/v1/firewalld"
	protocol "moi-quota/pkg/protocol/v1"
)

type firewalldContext struct {
	firewalld.UnimplementedFirewalldServiceServer
	firewalldRepository repository.RepositoryFirewalld
}

func (f firewalldContext) GetFirewall(ctx context.Context, in *emptypb.Empty) (*firewalld.ResponseFirewall, error) {
	quotaFirewalld, err := f.firewalldRepository.GetFirewall(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &firewalld.ResponseFirewall{
		FirewallQuota:        quotaFirewalld.Quota,
		FirewallUsage:        quotaFirewalld.Usage,
		FirewallUsagePercent: quotaFirewalld.GetUsagePercent(),
		FirewallFreePercent:  quotaFirewalld.GetFreePercent(),
	}, nil
}
func (f firewalldContext) SetFirewall(ctx context.Context, in *firewalld.RequestFirewall) (*firewalld.ResponseFirewall, error) {
	err := f.firewalldRepository.CreateQuotaFirewall(ctx, in)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	quotaFirewalld, err := f.firewalldRepository.GetFirewall(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &firewalld.ResponseFirewall{
		FirewallQuota:        quotaFirewalld.Quota,
		FirewallUsage:        quotaFirewalld.Usage,
		FirewallUsagePercent: quotaFirewalld.GetUsagePercent(),
		FirewallFreePercent:  quotaFirewalld.GetFreePercent(),
	}, nil
}

type firewalldService struct {
	service firewalld.FirewalldServiceServer
	ctx     context.Context
}

func (f firewalldService) TcpListening(server *grpc.Server) {
	firewalld.RegisterFirewalldServiceServer(server, f.service)
}

func (f firewalldService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return firewalld.RegisterFirewalldServiceHandler(f.ctx, mux, conn)
}

func initFirewalldService(repositoryFirewalld repository.RepositoryFirewalld) firewalld.FirewalldServiceServer {
	return &firewalldContext{
		firewalldRepository: repositoryFirewalld,
	}
}

func NewProtocolService(ctx context.Context, repositoryFirewalld repository.RepositoryFirewalld) protocol.Service {
	return &firewalldService{
		ctx:     ctx,
		service: initFirewalldService(repositoryFirewalld),
	}
}
