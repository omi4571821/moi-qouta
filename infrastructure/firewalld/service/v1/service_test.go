package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	v1 "moi-quota/infrastructure/firewalld/repository/v1"
	repositoryMock "moi-quota/mock/infrastructure/firewalld/repository/v1"
	"moi-quota/pkg/api/infrastructure/v1/firewalld"
	protocolV1 "moi-quota/pkg/protocol/v1"
	"reflect"
	"testing"
)

func Test_firewalldContext_GetFirewall(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxSuccess := context.Background()
	quotaSuccess := &v1.FirewalldQuota{
		Quota: 100,
		Usage: 50,
	}
	repositorySuccess.EXPECT().GetFirewall(ctxSuccess).Return(quotaSuccess, nil)
	wantSuccess := &firewalld.ResponseFirewall{
		FirewallQuota:        quotaSuccess.Quota,
		FirewallUsage:        quotaSuccess.Usage,
		FirewallUsagePercent: quotaSuccess.GetUsagePercent(),
		FirewallFreePercent:  quotaSuccess.GetUsagePercent(),
	}
	// empty case
	repositoryEmpty := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxEmpty := context.Background()
	quotaEmpty := &v1.FirewalldQuota{
		Quota: 0,
		Usage: 0,
	}
	repositoryEmpty.EXPECT().GetFirewall(ctxEmpty).Return(quotaEmpty, nil)
	wantEmpty := &firewalld.ResponseFirewall{
		FirewallQuota:        quotaEmpty.Quota,
		FirewallUsage:        quotaEmpty.Usage,
		FirewallUsagePercent: quotaEmpty.GetUsagePercent(),
		FirewallFreePercent:  quotaEmpty.GetUsagePercent(),
	}
	// error case
	repositoryError := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxError := context.Background()
	repositoryError.EXPECT().GetFirewall(ctxError).Return(nil, pkgMongo.ErrNilDocument)
	wantError := &firewalld.ResponseFirewall{}
	wantError = nil
	type fields struct {
		UnimplementedFirewalldServiceServer firewalld.UnimplementedFirewalldServiceServer
		firewalldRepository                 v1.RepositoryFirewalld
	}
	type args struct {
		ctx context.Context
		in  *emptypb.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *firewalld.ResponseFirewall
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  &emptypb.Empty{},
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "empty",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositoryEmpty,
			},
			args: args{
				ctx: ctxEmpty,
				in:  &emptypb.Empty{},
			},
			want: wantEmpty,
		},
		{
			name: "error",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositoryError,
			},
			args: args{
				ctx: ctxError,
			},
			want:    wantError,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := firewalldContext{
				UnimplementedFirewalldServiceServer: tt.fields.UnimplementedFirewalldServiceServer,
				firewalldRepository:                 tt.fields.firewalldRepository,
			}
			got, err := f.GetFirewall(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFirewall() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFirewall() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_firewalldContext_SetFirewall(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxSuccess := context.Background()
	quotaSuccess := &v1.FirewalldQuota{
		Quota: 100,
		Usage: 50,
	}
	inSuccess := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	repositorySuccess.EXPECT().CreateQuotaFirewall(ctxSuccess, inSuccess).Return(nil)
	repositorySuccess.EXPECT().GetFirewall(ctxSuccess).Return(quotaSuccess, nil)
	wantSuccess := &firewalld.ResponseFirewall{
		FirewallQuota:        quotaSuccess.Quota,
		FirewallUsage:        quotaSuccess.Usage,
		FirewallUsagePercent: quotaSuccess.GetUsagePercent(),
		FirewallFreePercent:  quotaSuccess.GetUsagePercent(),
	}
	// error case insert
	repositoryErrorInsert := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxErrorInsert := context.Background()
	inErrorInsert := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	repositoryErrorInsert.EXPECT().CreateQuotaFirewall(ctxErrorInsert, inErrorInsert).Return(pkgMongo.ErrWrongClient)
	wantErrorInsert := &firewalld.ResponseFirewall{}
	wantErrorInsert = nil
	// error case get
	repositoryErrorGet := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxErrorGet := context.Background()
	inErrorGet := &firewalld.RequestFirewall{
		RequestFirewallQuota: 100,
	}
	repositoryErrorGet.EXPECT().CreateQuotaFirewall(ctxErrorGet, inErrorGet).Return(nil)
	repositoryErrorGet.EXPECT().GetFirewall(ctxErrorGet).Return(nil, pkgMongo.ErrNilDocument)
	wantErrorGet := &firewalld.ResponseFirewall{}
	wantErrorGet = nil
	type fields struct {
		UnimplementedFirewalldServiceServer firewalld.UnimplementedFirewalldServiceServer
		firewalldRepository                 v1.RepositoryFirewalld
	}
	type args struct {
		ctx context.Context
		in  *firewalld.RequestFirewall
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *firewalld.ResponseFirewall
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "error insert",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositoryErrorInsert,
			},
			args: args{
				ctx: ctxErrorInsert,
				in:  inErrorInsert,
			},
			want:    wantErrorInsert,
			wantErr: true,
		},
		{
			name: "error get",
			fields: fields{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositoryErrorGet,
			},
			args: args{
				ctx: ctxErrorGet,
				in:  inErrorGet,
			},
			want:    wantErrorGet,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := firewalldContext{
				UnimplementedFirewalldServiceServer: tt.fields.UnimplementedFirewalldServiceServer,
				firewalldRepository:                 tt.fields.firewalldRepository,
			}
			got, err := f.SetFirewall(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("SetFirewall() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SetFirewall() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_firewalldService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxSuccess := context.Background()
	serviceSuccess := &firewalldContext{
		UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
		firewalldRepository:                 repositorySuccess,
	}
	type fields struct {
		service firewalld.FirewalldServiceServer
		ctx     context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				service: serviceSuccess,
				ctx:     ctxSuccess,
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := firewalldService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			f.TcpListening(tt.args.server)
		})
	}
}

func Test_firewalldService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxSuccess := context.Background()
	serviceSuccess := &firewalldContext{
		UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
		firewalldRepository:                 repositorySuccess,
	}
	type fields struct {
		service firewalld.FirewalldServiceServer
		ctx     context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				service: serviceSuccess,
				ctx:     ctxSuccess,
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := firewalldService{
				service: tt.fields.service,
				ctx:     tt.fields.ctx,
			}
			if err := f.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initFirewalldService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	type args struct {
		repositoryFirewalld v1.RepositoryFirewalld
	}
	tests := []struct {
		name string
		args args
		want firewalld.FirewalldServiceServer
	}{
		{
			name: "success",
			args: args{
				repositoryFirewalld: repositorySuccess,
			},
			want: &firewalldContext{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 repositorySuccess,
			},
		},
		{
			name: "nil",
			args: args{
				repositoryFirewalld: nil,
			},
			want: &firewalldContext{
				UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
				firewalldRepository:                 nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initFirewalldService(tt.args.repositoryFirewalld); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initFirewalldService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewProtocolService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryFirewalld(ctrl)
	ctxSuccess := context.Background()
	type args struct {
		ctx                 context.Context
		repositoryFirewalld v1.RepositoryFirewalld
	}
	tests := []struct {
		name string
		args args
		want protocolV1.Service
	}{
		{
			name: "success",
			args: args{
				ctx:                 ctxSuccess,
				repositoryFirewalld: repositorySuccess,
			},
			want: &firewalldService{
				service: &firewalldContext{
					UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
					firewalldRepository:                 repositorySuccess,
				},
				ctx: ctxSuccess,
			},
		},
		{
			name: "nil",
			args: args{
				ctx:                 ctxSuccess,
				repositoryFirewalld: nil,
			},
			want: &firewalldService{
				service: &firewalldContext{
					UnimplementedFirewalldServiceServer: firewalld.UnimplementedFirewalldServiceServer{},
					firewalldRepository:                 nil,
				},
				ctx: ctxSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewProtocolService(tt.args.ctx, tt.args.repositoryFirewalld); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewProtocolService() = %v, want %v", got, tt.want)
			}
		})
	}
}
