package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"moi-quota/driver/mongo"
)

const CollectionName = "platform_ocr"

type mongoContext struct {
	mongo.Collection
}

func (m mongoContext) CreateQuotaOCR(ctx context.Context, quota int64) error {
	quotaOCR, err := m.GetQuotaOCR(ctx)
	if err != nil {
		quotaOCR.Quota = quota
		quotaOCR.Used = 0
		_, err = m.Collection.InsertOne(ctx, quotaOCR)
	} else {
		quotaOCR.Quota += quota
		_, err = m.Collection.UpdateOne(ctx, bson.M{"_id": quotaOCR.Id}, bson.M{"$set": quotaOCR})
	}
	return err
}

func (m mongoContext) GetQuotaOCR(ctx context.Context) (*ORCQuota, error) {
	ocr := &ORCQuota{
		Quota: 0,
	}
	err := m.Collection.FindOne(ctx, bson.M{}).Decode(&ocr)
	return ocr, err
}

func NewMongoRepository(collection mongo.Collection) RepositoryPlatformOCR {
	return &mongoContext{
		Collection: collection,
	}
}
