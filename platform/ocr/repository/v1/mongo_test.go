package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	driverMongoMock "moi-quota/mock/driver/mongo"
	"reflect"
	"testing"
)

func Test_mongoContext_CreateQuotaOCR(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case insert
	collectionSuccessInsert := driverMongoMock.NewMockCollection(ctrl)
	singleCursorSuccessInsert := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccessInsert := context.Background()
	quotaSuccessInsert := int64(100)
	insertResultSuccessInsert := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	collectionSuccessInsert.EXPECT().InsertOne(ctxSuccessInsert, &ORCQuota{
		Quota: quotaSuccessInsert,
		Used:  0,
	}).Return(insertResultSuccessInsert, nil)
	gomock.InOrder(
		collectionSuccessInsert.EXPECT().FindOne(ctxSuccessInsert, bson.M{}).Return(singleCursorSuccessInsert),
		singleCursorSuccessInsert.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// success case update
	collectionSuccessUpdate := driverMongoMock.NewMockCollection(ctrl)
	singleCursorSuccessUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccessUpdate := context.Background()
	quotaSuccessUpdate := int64(100)
	idSuccessUpdate := primitive.NewObjectID()
	updateResultSuccessUpdate := &pkgMongo.UpdateResult{
		MatchedCount: 1,
	}
	quotaDataSuccessUpdate := &ORCQuota{
		Id:    idSuccessUpdate,
		Quota: 10,
		Used:  0,
	}
	collectionSuccessUpdate.EXPECT().UpdateOne(ctxSuccessUpdate, bson.M{"_id": idSuccessUpdate}, bson.M{"$set": &ORCQuota{
		Id:    idSuccessUpdate,
		Quota: 10 + quotaSuccessUpdate,
		Used:  0,
	}}).Return(updateResultSuccessUpdate, nil)
	gomock.InOrder(
		collectionSuccessUpdate.EXPECT().FindOne(ctxSuccessUpdate, bson.M{}).Return(singleCursorSuccessUpdate),
		singleCursorSuccessUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaDataSuccessUpdate))
			return nil
		}),
	)
	// failed case insert
	collectionFailedInsert := driverMongoMock.NewMockCollection(ctrl)
	singleCursorFailedInsert := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxFailedInsert := context.Background()
	quotaFailedInsert := int64(100)
	insertResultFailedInsert := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	insertResultFailedInsert = nil
	collectionFailedInsert.EXPECT().InsertOne(ctxFailedInsert, &ORCQuota{
		Quota: quotaFailedInsert,
		Used:  0,
	}).Return(insertResultFailedInsert, pkgMongo.ErrNilDocument)
	gomock.InOrder(
		collectionFailedInsert.EXPECT().FindOne(ctxFailedInsert, bson.M{}).Return(singleCursorFailedInsert),
		singleCursorFailedInsert.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// failed case update
	collectionFailedUpdate := driverMongoMock.NewMockCollection(ctrl)
	singleCursorFailedUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxFailedUpdate := context.Background()
	quotaFailedUpdate := int64(100)
	idFailedUpdate := primitive.NewObjectID()
	updateResultFailedUpdate := &pkgMongo.UpdateResult{
		MatchedCount: 0,
	}
	updateResultFailedUpdate = nil
	quotaDataFailedUpdate := &ORCQuota{
		Id:    idFailedUpdate,
		Quota: 10,
		Used:  0,
	}
	collectionFailedUpdate.EXPECT().UpdateOne(ctxFailedUpdate, bson.M{"_id": idFailedUpdate}, bson.M{"$set": &ORCQuota{
		Id:    idFailedUpdate,
		Quota: 10 + quotaFailedUpdate,
	}}).Return(updateResultFailedUpdate, pkgMongo.ErrClientDisconnected)
	gomock.InOrder(
		collectionFailedUpdate.EXPECT().FindOne(ctxFailedUpdate, bson.M{}).Return(singleCursorFailedUpdate),
		singleCursorFailedUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaDataFailedUpdate))
			return nil
		}),
	)
	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx   context.Context
		quota int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: collectionSuccessInsert,
			},
			args: args{
				ctx:   ctxSuccessInsert,
				quota: quotaSuccessInsert,
			},
			wantErr: false,
		},
		{
			name: "success update",
			fields: fields{
				Collection: collectionSuccessUpdate,
			},
			args: args{
				ctx:   ctxSuccessUpdate,
				quota: quotaSuccessUpdate,
			},
			wantErr: false,
		},
		{
			name: "failed insert",
			fields: fields{
				Collection: collectionFailedInsert,
			},
			args: args{
				ctx:   ctxFailedInsert,
				quota: quotaFailedInsert,
			},
			wantErr: true,
		},
		{
			name: "failed update",
			fields: fields{
				Collection: collectionFailedUpdate,
			},
			args: args{
				ctx:   ctxFailedUpdate,
				quota: quotaFailedUpdate,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if err := m.CreateQuotaOCR(tt.args.ctx, tt.args.quota); (err != nil) != tt.wantErr {
				t.Errorf("CreateQuotaOCR() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMongoRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	collectionSuccess := driverMongoMock.NewMockCollection(ctrl)
	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want RepositoryPlatformOCR
	}{
		{
			name: "success",
			args: args{
				collection: collectionSuccess,
			},
			want: &mongoContext{
				Collection: collectionSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
