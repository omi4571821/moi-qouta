package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ORCQuota struct {
	Id    primitive.ObjectID `bson:"_id,omitempty" json:"id,omitempty"`
	Quota int64              `bson:"quota,omitempty" json:"quota,omitempty"`
	Used  int64              `bson:"used,omitempty" json:"used,omitempty"`
}

func (q ORCQuota) GetUsedPercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return float32(q.Used) / float32(q.Quota) * 100
}

func (q ORCQuota) GetFreePercent() float32 {
	if q.Quota == 0 {
		return 0
	}
	return float32(q.Quota-q.Used) / float32(q.Quota) * 100
}

type RepositoryPlatformOCR interface {
	CreateQuotaOCR(ctx context.Context, quota int64) error
	GetQuotaOCR(ctx context.Context) (*ORCQuota, error)
}
