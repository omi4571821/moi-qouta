package v1

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

func TestORCQuota_GetUsedPercent(t *testing.T) {
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Used  int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Used Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Used:  50,
			},
			want: 50,
		},
		{
			name: "Test Used Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Used:  0,
			},
			want: 0,
		},
		{
			name: "Test Used Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Used:  50,
			},
			want: 0,
		},
		{
			name: "Test Used Percent Usage Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Used:  0,
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := ORCQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Used:  tt.fields.Used,
			}
			if got := q.GetUsedPercent(); got != tt.want {
				t.Errorf("GetUsedPercent() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestORCQuota_GetFreePercent(t *testing.T) {
	type fields struct {
		Id    primitive.ObjectID
		Quota int64
		Used  int64
	}
	tests := []struct {
		name   string
		fields fields
		want   float32
	}{
		{
			name: "Test Free Percent",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Used:  50,
			},
			want: 50,
		},
		{
			name: "Test Free Percent Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Used:  0,
			},
			want: 0,
		},
		{
			name: "Test Free Percent Quota Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 0,
				Used:  50,
			},
			want: 0,
		},
		{
			name: "Test Free Percent Usage Zero",
			fields: fields{
				Id:    primitive.NewObjectID(),
				Quota: 100,
				Used:  0,
			},
			want: 100,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := ORCQuota{
				Id:    tt.fields.Id,
				Quota: tt.fields.Quota,
				Used:  tt.fields.Used,
			}
			if got := q.GetFreePercent(); got != tt.want {
				t.Errorf("GetFreePercent() = %v, want %v", got, tt.want)
			}
		})
	}
}
