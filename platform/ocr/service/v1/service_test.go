package v1

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	repositoryMock "moi-quota/mock/platform/ocr/repository/v1"
	"moi-quota/pkg/api/platform/v1/ocr"
	protocolV1 "moi-quota/pkg/protocol/v1"
	v1 "moi-quota/platform/ocr/repository/v1"
	"reflect"
	"testing"
)

func Test_ocrContext_SetOCRQuota(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxSuccess := context.Background()
	quotaSuccess := &v1.ORCQuota{
		Quota: 100,
		Used:  50,
	}

	inputSuccess := int64(100)
	inQuotaSuccess := &ocr.RequestQuotaOCR{
		RequestUserQuota: inputSuccess,
	}
	repositorySuccess.EXPECT().CreateQuotaOCR(ctxSuccess, inputSuccess).Return(nil)
	repositorySuccess.EXPECT().GetQuotaOCR(ctxSuccess).Return(quotaSuccess, nil)
	wantSuccess := &ocr.ResponseQuotaOCR{
		UserQuota:            quotaSuccess.Quota,
		UserQuotaUsed:        quotaSuccess.Used,
		UserQuotaUsedPercent: quotaSuccess.GetUsedPercent(),
		UserQuotaFreePercent: quotaSuccess.GetFreePercent(),
	}
	// fail case insert
	repositoryFailInsert := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxFailInsert := context.Background()
	inputFailInsert := int64(100)
	inQuotaFailInsert := &ocr.RequestQuotaOCR{
		RequestUserQuota: inputFailInsert,
	}
	errInsert := pkgMongo.ErrClientDisconnected
	repositoryFailInsert.EXPECT().CreateQuotaOCR(ctxFailInsert, inputFailInsert).Return(errInsert)
	wantFailInsert := &ocr.ResponseQuotaOCR{}
	wantFailInsert = nil
	// fail case get
	repositoryFailGet := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxFailGet := context.Background()
	inputFailGet := int64(100)
	inQuotaFailGet := &ocr.RequestQuotaOCR{
		RequestUserQuota: inputFailGet,
	}
	errGet := pkgMongo.ErrNilDocument
	repositoryFailGet.EXPECT().CreateQuotaOCR(ctxFailGet, inputFailGet).Return(nil)
	repositoryFailGet.EXPECT().GetQuotaOCR(ctxFailGet).Return(nil, errGet)
	wantFailGet := &ocr.ResponseQuotaOCR{}
	wantFailGet = nil
	type fields struct {
		UnimplementedOCRPlatformServiceServer ocr.UnimplementedOCRPlatformServiceServer
		RepositoryPlatformOCR                 v1.RepositoryPlatformOCR
	}
	type args struct {
		ctx context.Context
		in  *ocr.RequestQuotaOCR
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *ocr.ResponseQuotaOCR
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{},
				RepositoryPlatformOCR:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inQuotaSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "fail insert",
			fields: fields{
				UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{},
				RepositoryPlatformOCR:                 repositoryFailInsert,
			},
			args: args{
				ctx: ctxFailInsert,
				in:  inQuotaFailInsert,
			},
			want:    wantFailInsert,
			wantErr: true,
		},
		{
			name: "fail get",
			fields: fields{
				UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{},
				RepositoryPlatformOCR:                 repositoryFailGet,
			},
			args: args{
				ctx: ctxFailGet,
				in:  inQuotaFailGet,
			},
			want:    wantFailGet,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ocrContext{
				UnimplementedOCRPlatformServiceServer: tt.fields.UnimplementedOCRPlatformServiceServer,
				RepositoryPlatformOCR:                 tt.fields.RepositoryPlatformOCR,
			}
			got, err := o.SetOCRQuota(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("SetOCRQuota() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SetOCRQuota() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ocrContext_GetOCRQuota(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxSuccess := context.Background()
	quotaSuccess := &v1.ORCQuota{
		Quota: 100,
		Used:  50,
	}
	repositorySuccess.EXPECT().GetQuotaOCR(ctxSuccess).Return(quotaSuccess, nil)
	wantSuccess := &ocr.ResponseQuotaOCR{
		UserQuota:            quotaSuccess.Quota,
		UserQuotaUsed:        quotaSuccess.Used,
		UserQuotaUsedPercent: quotaSuccess.GetUsedPercent(),
		UserQuotaFreePercent: quotaSuccess.GetFreePercent(),
	}
	// fail case
	repositoryFail := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxFail := context.Background()
	errFail := pkgMongo.ErrNilDocument
	repositoryFail.EXPECT().GetQuotaOCR(ctxFail).Return(nil, errFail)
	wantFail := &ocr.ResponseQuotaOCR{}
	wantFail = nil
	type fields struct {
		UnimplementedOCRPlatformServiceServer ocr.UnimplementedOCRPlatformServiceServer
		RepositoryPlatformOCR                 v1.RepositoryPlatformOCR
	}
	type args struct {
		ctx context.Context
		in  *emptypb.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *ocr.ResponseQuotaOCR
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{},
				RepositoryPlatformOCR:                 repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  &emptypb.Empty{},
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "fail",
			fields: fields{
				UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{},
				RepositoryPlatformOCR:                 repositoryFail,
			},
			args: args{
				ctx: ctxFail,
				in:  &emptypb.Empty{},
			},
			want:    wantFail,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ocrContext{
				UnimplementedOCRPlatformServiceServer: tt.fields.UnimplementedOCRPlatformServiceServer,
				RepositoryPlatformOCR:                 tt.fields.RepositoryPlatformOCR,
			}
			got, err := o.GetOCRQuota(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetOCRQuota() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOCRQuota() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ocrService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxSuccess := context.Background()

	type fields struct {
		Context                  context.Context
		OCRPlatformServiceServer ocr.OCRPlatformServiceServer
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				Context:                  ctxSuccess,
				OCRPlatformServiceServer: &ocrContext{RepositoryPlatformOCR: repositorySuccess},
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ocrService{
				Context:                  tt.fields.Context,
				OCRPlatformServiceServer: tt.fields.OCRPlatformServiceServer,
			}
			o.TcpListening(tt.args.server)
		})
	}
}

func Test_ocrService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	type fields struct {
		Context                  context.Context
		OCRPlatformServiceServer ocr.OCRPlatformServiceServer
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Context:                  context.Background(),
				OCRPlatformServiceServer: &ocrContext{RepositoryPlatformOCR: repositorySuccess},
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := ocrService{
				Context:                  tt.fields.Context,
				OCRPlatformServiceServer: tt.fields.OCRPlatformServiceServer,
			}
			if err := o.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initOCRService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)

	type args struct {
		repositoryPlatformOCR v1.RepositoryPlatformOCR
	}
	tests := []struct {
		name string
		args args
		want ocr.OCRPlatformServiceServer
	}{
		{
			name: "success",
			args: args{
				repositoryPlatformOCR: repositorySuccess,
			},
			want: &ocrContext{RepositoryPlatformOCR: repositorySuccess},
		},
		{
			name: "nil",
			args: args{
				repositoryPlatformOCR: nil,
			},
			want: &ocrContext{RepositoryPlatformOCR: nil},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initOCRService(tt.args.repositoryPlatformOCR); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initOCRService() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewOCRProtoService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformOCR(ctrl)
	ctxSuccess := context.Background()
	serivceSuccess := &ocrContext{RepositoryPlatformOCR: repositorySuccess, UnimplementedOCRPlatformServiceServer: ocr.UnimplementedOCRPlatformServiceServer{}}

	type args struct {
		ctx                   context.Context
		repositoryPlatformOCR v1.RepositoryPlatformOCR
	}
	tests := []struct {
		name string
		args args
		want protocolV1.Service
	}{
		{
			name: "success",
			args: args{
				ctx:                   ctxSuccess,
				repositoryPlatformOCR: repositorySuccess,
			},
			want: &ocrService{
				Context:                  ctxSuccess,
				OCRPlatformServiceServer: serivceSuccess,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewOCRProtoService(tt.args.ctx, tt.args.repositoryPlatformOCR); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewOCRProtoService() = %v, want %v", got, tt.want)
			}
		})
	}
}
