package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"moi-quota/pkg/api/platform/v1/ocr"
	protocol "moi-quota/pkg/protocol/v1"
	repository "moi-quota/platform/ocr/repository/v1"
)

type ocrContext struct {
	ocr.UnimplementedOCRPlatformServiceServer
	repository.RepositoryPlatformOCR
}

func (o ocrContext) SetOCRQuota(ctx context.Context, in *ocr.RequestQuotaOCR) (*ocr.ResponseQuotaOCR, error) {
	var err = o.RepositoryPlatformOCR.CreateQuotaOCR(ctx, in.RequestUserQuota)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	quota, err := o.RepositoryPlatformOCR.GetQuotaOCR(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &ocr.ResponseQuotaOCR{
		UserQuota:            quota.Quota,
		UserQuotaUsed:        quota.Used,
		UserQuotaUsedPercent: quota.GetUsedPercent(),
		UserQuotaFreePercent: quota.GetFreePercent(),
	}, nil
}
func (o ocrContext) GetOCRQuota(ctx context.Context, in *emptypb.Empty) (*ocr.ResponseQuotaOCR, error) {
	quota, err := o.RepositoryPlatformOCR.GetQuotaOCR(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &ocr.ResponseQuotaOCR{
		UserQuota:            quota.Quota,
		UserQuotaUsed:        quota.Used,
		UserQuotaUsedPercent: quota.GetUsedPercent(),
		UserQuotaFreePercent: quota.GetFreePercent(),
	}, nil
}

type ocrService struct {
	context.Context
	ocr.OCRPlatformServiceServer
}

func (o ocrService) TcpListening(server *grpc.Server) {
	ocr.RegisterOCRPlatformServiceServer(server, o.OCRPlatformServiceServer)
}

func (o ocrService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return ocr.RegisterOCRPlatformServiceHandler(o.Context, mux, conn)
}

func initOCRService(repositoryPlatformOCR repository.RepositoryPlatformOCR) ocr.OCRPlatformServiceServer {
	return &ocrContext{
		RepositoryPlatformOCR: repositoryPlatformOCR,
	}
}

func NewOCRProtoService(ctx context.Context, repositoryPlatformOCR repository.RepositoryPlatformOCR) protocol.Service {
	return &ocrService{
		ctx,
		initOCRService(repositoryPlatformOCR),
	}
}
