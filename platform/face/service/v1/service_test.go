package v1

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	repositoryMock "moi-quota/mock/platform/face/repository/v1"
	"moi-quota/pkg/api/platform/v1/face"
	protocolV1 "moi-quota/pkg/protocol/v1"
	v1 "moi-quota/platform/face/repository/v1"
	"reflect"
	"testing"
)

func Test_faceContext_GetQuotaFace(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	quotaSuccess := v1.FaceQuota{
		Quota: 100,
		Used:  50,
	}
	repositorySuccess.EXPECT().GetQuotaFace(gomock.Any()).Return(&quotaSuccess, nil)
	ctxSuccess := context.Background()
	wantSuccess := &face.ResponseQuotaFace{
		UserQuota:       quotaSuccess.Quota,
		UserUsed:        quotaSuccess.Used,
		UserUsedPercent: quotaSuccess.GetUsedPercent(),
		UserFreePercent: quotaSuccess.GetUsedPercent(),
	}
	// empty case
	repositoryEmpty := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	quotaEmpty := v1.FaceQuota{
		Quota: 0,
		Used:  0,
	}
	repositoryEmpty.EXPECT().GetQuotaFace(gomock.Any()).Return(&quotaEmpty, nil)
	ctxEmpty := context.Background()
	wantEmpty := &face.ResponseQuotaFace{
		UserQuota:       quotaEmpty.Quota,
		UserUsed:        quotaEmpty.Used,
		UserUsedPercent: quotaEmpty.GetUsedPercent(),
		UserFreePercent: quotaEmpty.GetUsedPercent(),
	}
	// error case
	repositoryError := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	repositoryError.EXPECT().GetQuotaFace(gomock.Any()).Return(nil, errors.New("error"))
	ctxError := context.Background()
	wantError := &face.ResponseQuotaFace{}
	wantError = nil
	type fields struct {
		UnimplementedQuotaFaceServiceServer face.UnimplementedQuotaFaceServiceServer
		RepositoryPlatformFace              v1.RepositoryPlatformFace
	}
	type args struct {
		ctx context.Context
		in  *emptypb.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *face.ResponseQuotaFace
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  &emptypb.Empty{},
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "empty",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositoryEmpty,
			},
			args: args{
				ctx: ctxEmpty,
				in:  &emptypb.Empty{},
			},
			want:    wantEmpty,
			wantErr: false,
		},
		{
			name: "error",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositoryError,
			},
			args: args{
				ctx: ctxError,
				in:  &emptypb.Empty{},
			},
			want:    wantError,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := faceContext{
				UnimplementedQuotaFaceServiceServer: tt.fields.UnimplementedQuotaFaceServiceServer,
				RepositoryPlatformFace:              tt.fields.RepositoryPlatformFace,
			}
			got, err := f.GetQuotaFace(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetQuotaFace() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetQuotaFace() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_faceContext_SetQuotaFace(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	quotaSuccess := v1.FaceQuota{
		Quota: 1000,
		Used:  50,
	}
	ctxSuccess := context.Background()
	quotaInput := int64(1000)
	inSuccess := &face.RequestQuotaFace{
		RequestUserQuota: quotaInput,
	}
	repositorySuccess.EXPECT().CreateQuotaFace(ctxSuccess, quotaInput).Return(nil)
	repositorySuccess.EXPECT().GetQuotaFace(ctxSuccess).Return(&quotaSuccess, nil)
	wantSuccess := &face.ResponseQuotaFace{
		UserQuota:       quotaSuccess.Quota,
		UserUsed:        quotaSuccess.Used,
		UserUsedPercent: quotaSuccess.GetUsedPercent(),
		UserFreePercent: quotaSuccess.GetFreePercent(),
	}
	// error case insert
	repositoryErrorInsert := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	ctxErrorInsert := context.Background()
	quotaInputErrorInsert := int64(1000)
	inErrorInsert := &face.RequestQuotaFace{
		RequestUserQuota: quotaInputErrorInsert,
	}
	repositoryErrorInsert.EXPECT().CreateQuotaFace(ctxErrorInsert, quotaInputErrorInsert).Return(errors.New("error"))
	wantErrorInsert := &face.ResponseQuotaFace{}
	wantErrorInsert = nil
	// error case get
	repositoryErrorGet := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	ctxErrorGet := context.Background()
	quotaInputErrorGet := int64(1000)
	inErrorGet := &face.RequestQuotaFace{
		RequestUserQuota: quotaInputErrorGet,
	}
	repositoryErrorGet.EXPECT().CreateQuotaFace(ctxErrorGet, quotaInputErrorGet).Return(nil)
	repositoryErrorGet.EXPECT().GetQuotaFace(ctxErrorGet).Return(nil, errors.New("error"))
	type fields struct {
		UnimplementedQuotaFaceServiceServer face.UnimplementedQuotaFaceServiceServer
		RepositoryPlatformFace              v1.RepositoryPlatformFace
	}
	type args struct {
		ctx context.Context
		in  *face.RequestQuotaFace
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *face.ResponseQuotaFace
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositorySuccess,
			},
			args: args{
				ctx: ctxSuccess,
				in:  inSuccess,
			},
			want:    wantSuccess,
			wantErr: false,
		},
		{
			name: "error insert",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositoryErrorInsert,
			},
			args: args{
				ctx: ctxErrorInsert,
				in:  inErrorInsert,
			},
			want:    wantErrorInsert,
			wantErr: true,
		},
		{
			name: "error get",
			fields: fields{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositoryErrorGet,
			},
			args: args{
				ctx: ctxErrorGet,
				in:  inErrorGet,
			},
			want:    wantErrorInsert,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := faceContext{
				UnimplementedQuotaFaceServiceServer: tt.fields.UnimplementedQuotaFaceServiceServer,
				RepositoryPlatformFace:              tt.fields.RepositoryPlatformFace,
			}
			got, err := f.SetQuotaFace(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("SetQuotaFace() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SetQuotaFace() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_faceService_TcpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	type fields struct {
		QuotaFaceServiceServer face.QuotaFaceServiceServer
		ctx                    context.Context
	}
	type args struct {
		server *grpc.Server
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success with unimplemented server",
			fields: fields{
				QuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				ctx:                    context.Background(),
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
		{
			name: "success with implemented server",
			fields: fields{
				QuotaFaceServiceServer: &faceContext{
					UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
					RepositoryPlatformFace:              repositorySuccess,
				},
				ctx: context.Background(),
			},
			args: args{
				server: grpc.NewServer(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := faceService{
				QuotaFaceServiceServer: tt.fields.QuotaFaceServiceServer,
				ctx:                    tt.fields.ctx,
			}
			f.TcpListening(tt.args.server)
		})
	}
}

func Test_faceService_HttpListening(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	type fields struct {
		QuotaFaceServiceServer face.QuotaFaceServiceServer
		ctx                    context.Context
	}
	type args struct {
		mux  *runtime.ServeMux
		conn *grpc.ClientConn
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "success with unimplemented server",
			fields: fields{
				QuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				ctx:                    context.Background(),
			},
			args: args{
				mux:  runtime.NewServeMux(),
				conn: nil,
			},
			wantErr: false,
		},
		{
			name: "success with implemented server",
			fields: fields{
				QuotaFaceServiceServer: &faceContext{
					UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
					RepositoryPlatformFace:              repositorySuccess,
				},
				ctx: context.Background(),
			},
			args: args{
				mux: runtime.NewServeMux(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := faceService{
				QuotaFaceServiceServer: tt.fields.QuotaFaceServiceServer,
				ctx:                    tt.fields.ctx,
			}
			if err := f.HttpListening(tt.args.mux, tt.args.conn); (err != nil) != tt.wantErr {
				t.Errorf("HttpListening() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_initFaceServiceServer(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	type args struct {
		platformFace v1.RepositoryPlatformFace
	}
	tests := []struct {
		name string
		args args
		want face.QuotaFaceServiceServer
	}{
		{
			name: "success",
			args: args{
				platformFace: repositorySuccess,
			},
			want: &faceContext{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              repositorySuccess,
			},
		},
		{
			name: "success with nil",
			args: args{
				platformFace: nil,
			},
			want: &faceContext{
				UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
				RepositoryPlatformFace:              nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := initFaceServiceServer(tt.args.platformFace); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("initFaceServiceServer() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewFaceProtoService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	repositorySuccess := repositoryMock.NewMockRepositoryPlatformFace(ctrl)
	ctxSuccess := context.Background()
	serviceSuccess := &faceService{
		QuotaFaceServiceServer: &faceContext{
			UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
			RepositoryPlatformFace:              repositorySuccess,
		},
		ctx: ctxSuccess,
	}
	// nil case
	ctxNil := context.Background()
	serviceNil := &faceService{
		QuotaFaceServiceServer: &faceContext{
			UnimplementedQuotaFaceServiceServer: face.UnimplementedQuotaFaceServiceServer{},
			RepositoryPlatformFace:              nil,
		},
		ctx: ctxNil,
	}
	type args struct {
		ctx          context.Context
		platformFace v1.RepositoryPlatformFace
	}
	tests := []struct {
		name string
		args args
		want protocolV1.Service
	}{
		{
			name: "success",
			args: args{
				ctx:          ctxSuccess,
				platformFace: repositorySuccess,
			},
			want: serviceSuccess,
		},
		{
			name: "success with nil",
			args: args{
				ctx:          ctxNil,
				platformFace: nil,
			},
			want: serviceNil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewFaceProtoService(tt.args.ctx, tt.args.platformFace); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewFaceProtoService() = %v, want %v", got, tt.want)
			}
		})
	}
}
