package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"moi-quota/pkg/api/platform/v1/face"
	protocol "moi-quota/pkg/protocol/v1"
	repository "moi-quota/platform/face/repository/v1"
)

type faceContext struct {
	face.UnimplementedQuotaFaceServiceServer
	repository.RepositoryPlatformFace
}

func (f faceContext) GetQuotaFace(ctx context.Context, in *emptypb.Empty) (*face.ResponseQuotaFace, error) {
	quota, err := f.RepositoryPlatformFace.GetQuotaFace(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &face.ResponseQuotaFace{
		UserQuota:       quota.Quota,
		UserUsed:        quota.Used,
		UserUsedPercent: quota.GetUsedPercent(),
		UserFreePercent: quota.GetFreePercent(),
	}, nil
}
func (f faceContext) SetQuotaFace(ctx context.Context, in *face.RequestQuotaFace) (*face.ResponseQuotaFace, error) {
	err := f.RepositoryPlatformFace.CreateQuotaFace(ctx, in.RequestUserQuota)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	quota, err := f.RepositoryPlatformFace.GetQuotaFace(ctx)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	return &face.ResponseQuotaFace{
		UserQuota:       quota.Quota,
		UserUsed:        quota.Used,
		UserUsedPercent: quota.GetUsedPercent(),
		UserFreePercent: quota.GetFreePercent(),
	}, nil
}

type faceService struct {
	face.QuotaFaceServiceServer
	ctx context.Context
}

func (f faceService) TcpListening(server *grpc.Server) {
	face.RegisterQuotaFaceServiceServer(server, f.QuotaFaceServiceServer)
}

func (f faceService) HttpListening(mux *runtime.ServeMux, conn *grpc.ClientConn) error {
	return face.RegisterQuotaFaceServiceHandler(f.ctx, mux, conn)
}

func initFaceServiceServer(platformFace repository.RepositoryPlatformFace) face.QuotaFaceServiceServer {
	return &faceContext{
		RepositoryPlatformFace: platformFace,
	}
}

func NewFaceProtoService(ctx context.Context, platformFace repository.RepositoryPlatformFace) protocol.Service {
	return &faceService{
		initFaceServiceServer(platformFace),
		ctx,
	}
}
