package v1

import (
	"context"
	"errors"
	"github.com/golang/mock/gomock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	pkgMongo "go.mongodb.org/mongo-driver/mongo"
	"moi-quota/driver/mongo"
	driverMongoMock "moi-quota/mock/driver/mongo"
	"reflect"
	"testing"
)

func Test_mongoContext_GetQuotaFace(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case
	collectionSuccess := driverMongoMock.NewMockCollection(ctrl)
	singResultSuccess := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccess := context.Background()
	wantSuccess := &FaceQuota{
		Quota: 100,
		Used:  50,
	}
	gomock.InOrder(
		collectionSuccess.EXPECT().FindOne(ctxSuccess, bson.M{}).Return(singResultSuccess),
		singResultSuccess.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(wantSuccess))
			return nil
		}),
	)
	// empty case
	collectionEmpty := driverMongoMock.NewMockCollection(ctrl)
	singResultEmpty := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxEmpty := context.Background()
	wantEmpty := &FaceQuota{}
	gomock.InOrder(
		collectionEmpty.EXPECT().FindOne(ctxEmpty, bson.M{}).Return(singResultEmpty),
		singResultEmpty.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *FaceQuota
		wantErr bool
	}{
		{
			name: "success",
			fields: fields{
				Collection: collectionSuccess,
			},
			args: args{
				ctx: ctxSuccess,
			},
			want: wantSuccess,
		},
		{
			name: "empty",
			fields: fields{
				Collection: collectionEmpty,
			},
			args: args{
				ctx: ctxEmpty,
			},
			want:    wantEmpty,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			got, err := m.GetQuotaFace(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetQuotaFace() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetQuotaFace() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mongoContext_CreateQuotaFace(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	// success case insert
	collectionSuccessInsert := driverMongoMock.NewMockCollection(ctrl)
	singResultSuccessInsert := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccessInsert := context.Background()
	quotaSuccessInsert := int64(100)
	resultInsertSuccess := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	collectionSuccessInsert.EXPECT().InsertOne(ctxSuccessInsert, &FaceQuota{
		Quota: quotaSuccessInsert,
		Used:  0,
	}).Return(resultInsertSuccess, nil)
	gomock.InOrder(
		collectionSuccessInsert.EXPECT().FindOne(ctxSuccessInsert, bson.M{}).Return(singResultSuccessInsert),
		singResultSuccessInsert.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// success case update
	collectionSuccessUpdate := driverMongoMock.NewMockCollection(ctrl)
	singResultSuccessUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxSuccessUpdate := context.Background()
	quotaSuccessUpdate := int64(100)
	idSuccessUpdate := primitive.NewObjectID()
	resultUpdateSuccess := &pkgMongo.UpdateResult{
		MatchedCount: 1,
	}
	quotaDataSuccessUpdate := &FaceQuota{
		Id:    idSuccessUpdate,
		Quota: 10,
		Used:  0,
	}
	collectionSuccessUpdate.EXPECT().UpdateOne(ctxSuccessUpdate, bson.M{"_id": idSuccessUpdate}, bson.M{"$set": &FaceQuota{
		Id:    idSuccessUpdate,
		Quota: 10 + quotaSuccessUpdate,
		Used:  0,
	}}).Return(resultUpdateSuccess, nil)
	gomock.InOrder(
		collectionSuccessUpdate.EXPECT().FindOne(ctxSuccessUpdate, bson.M{}).Return(singResultSuccessUpdate),
		singResultSuccessUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaDataSuccessUpdate))
			return nil
		}),
	)
	// error case insert
	collectionErrorInsert := driverMongoMock.NewMockCollection(ctrl)
	singResultErrorInsert := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxErrorInsert := context.Background()
	quotaErrorInsert := int64(100)
	resultInsertError := &pkgMongo.InsertOneResult{
		InsertedID: primitive.NewObjectID(),
	}
	collectionErrorInsert.EXPECT().InsertOne(ctxErrorInsert, &FaceQuota{
		Quota: quotaErrorInsert,
		Used:  0,
	}).Return(resultInsertError, errors.New("error"))
	gomock.InOrder(
		collectionErrorInsert.EXPECT().FindOne(ctxErrorInsert, bson.M{}).Return(singResultErrorInsert),
		singResultErrorInsert.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			return pkgMongo.ErrNilDocument
		}),
	)
	// error case update
	collectionErrorUpdate := driverMongoMock.NewMockCollection(ctrl)
	singResultErrorUpdate := driverMongoMock.NewMockSingleResultDecoder(ctrl)
	ctxErrorUpdate := context.Background()
	quotaErrorUpdate := int64(100)
	idErrorUpdate := primitive.NewObjectID()
	resultUpdateError := &pkgMongo.UpdateResult{
		MatchedCount: 0,
	}
	quotaDataErrorUpdate := &FaceQuota{
		Id:    idErrorUpdate,
		Quota: 10,
		Used:  0,
	}
	collectionErrorUpdate.EXPECT().UpdateOne(ctxErrorUpdate, bson.M{"_id": idErrorUpdate}, bson.M{
		"$set": &FaceQuota{
			Id:    idErrorUpdate,
			Quota: 10 + quotaErrorUpdate,
			Used:  0,
		},
	},
	).Return(resultUpdateError, errors.New("error"))
	gomock.InOrder(
		collectionErrorUpdate.EXPECT().FindOne(ctxErrorUpdate, bson.M{}).Return(singResultErrorUpdate),
		singResultErrorUpdate.EXPECT().Decode(gomock.Any()).DoAndReturn(func(i interface{}) error {
			reflect.ValueOf(i).Elem().Set(reflect.ValueOf(quotaDataErrorUpdate))
			return nil
		}),
	)

	type fields struct {
		Collection mongo.Collection
	}
	type args struct {
		ctx   context.Context
		quota int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			// success case insert
			name: "success insert",
			fields: fields{
				Collection: collectionSuccessInsert,
			},
			args: args{
				ctx:   ctxSuccessInsert,
				quota: quotaSuccessInsert,
			},
			wantErr: false,
		},
		{
			// success case update
			name: "success update",
			fields: fields{
				Collection: collectionSuccessUpdate,
			},
			args: args{
				ctx:   ctxSuccessUpdate,
				quota: quotaSuccessUpdate,
			},
			wantErr: false,
		},
		{
			// error case insert
			name: "error insert",
			fields: fields{
				Collection: collectionErrorInsert,
			},
			args: args{
				ctx:   ctxErrorInsert,
				quota: quotaErrorInsert,
			},
			wantErr: true,
		},
		{
			// error case update
			name: "error update",
			fields: fields{
				Collection: collectionErrorUpdate,
			},
			args: args{
				ctx:   ctxErrorUpdate,
				quota: quotaErrorUpdate,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := mongoContext{
				Collection: tt.fields.Collection,
			}
			if err := m.CreateQuotaFace(tt.args.ctx, tt.args.quota); (err != nil) != tt.wantErr {
				t.Errorf("CreateQuotaFace() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNewMongoRepository(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	collection := driverMongoMock.NewMockCollection(ctrl)
	type args struct {
		collection mongo.Collection
	}
	tests := []struct {
		name string
		args args
		want RepositoryPlatformFace
	}{
		{
			name: "success",
			args: args{
				collection: collection,
			},
			want: &mongoContext{
				Collection: collection,
			},
		},
		{
			name: "nil collection",
			args: args{
				collection: nil,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewMongoRepository(tt.args.collection); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMongoRepository() = %v, want %v", got, tt.want)
			}
		})
	}
}
