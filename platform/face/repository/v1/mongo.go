package v1

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"moi-quota/driver/mongo"
)

const CollectionName = "platform_face"

type mongoContext struct {
	mongo.Collection
}

func (m mongoContext) GetQuotaFace(ctx context.Context) (*FaceQuota, error) {
	face := &FaceQuota{
		Quota: 0,
	}
	err := m.Collection.FindOne(ctx, bson.M{}).Decode(&face)
	return face, err
}

func (m mongoContext) CreateQuotaFace(ctx context.Context, quota int64) error {
	face, err := m.GetQuotaFace(ctx)
	if err != nil {
		face.Quota = quota
		face.Used = 0
		_, err = m.Collection.InsertOne(ctx, face)
	} else {
		face.Quota += quota
		_, err = m.Collection.UpdateOne(ctx, bson.M{"_id": face.Id}, bson.M{"$set": face})
	}
	return err
}

func NewMongoRepository(collection mongo.Collection) RepositoryPlatformFace {
	if collection == nil {
		return nil
	}
	return &mongoContext{
		Collection: collection,
	}
}
