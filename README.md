# MOI Quota Service
## Description
 Quota service for MOI project to manage the quota of the service.
## Installation
### Prerequisites
- Goland 1.19
- Docker
- Docker-compose
### Steps
1. Clone the project
2. Run the following command to start the service
```bash
docker-compose up
```
## Usage
### API
#### Add a new quota virtual machine
```bash
curl --location --request POST 'http://localhost:8080/quota/vm/v1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "requestMemory": 10,
    "requestCpu": 10,
    "requestStorage": 1000
}
'
```

#### Get all quota virtual machines
```bash
curl --location --request GET 'http://localhost:8080/quota/vm/v1'
```

#### Add a new quota firewall

```bash
curl --location --request POST 'http://localhost:8080/quota/firewall/v1' \
--header 'Content-Type: application/json' \
--data-raw '{
  "requestFirewallQuota": 10
}'
```

#### Get all quota firewalls
```bash
curl --location --request GET 'http://localhost:8080/quota/firewall/v1'
```

#### Add a new quota load balancer
```bash
curl --location --request POST 'http://localhost:8080/quota/loadbalancer/v1' \
--header 'Content-Type: application/json' \
--data-raw '{
  "requestLoadBalanceCount": 10
}'
```

#### Get all quota load balancers
```bash
curl --location --request GET 'http://localhost:8080/quota/loadbalancer/v1'
```

