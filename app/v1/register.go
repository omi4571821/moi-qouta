package v1

import (
	"context"
	mongoDriver "moi-quota/driver/mongo"
	firewalldRepository "moi-quota/infrastructure/firewalld/repository/v1"
	firewalld "moi-quota/infrastructure/firewalld/service/v1"
	loadBalanceRepository "moi-quota/infrastructure/load_balance/repository/v1"
	loadBalance "moi-quota/infrastructure/load_balance/service/v1"
	virtualMachineInstanceRepository "moi-quota/infrastructure/virtual_machine/repository/v1/instance"
	virtualMachineQuotaRepository "moi-quota/infrastructure/virtual_machine/repository/v1/quota"
	vm "moi-quota/infrastructure/virtual_machine/service/v1"
	protocol "moi-quota/pkg/protocol/v1"
	faceRepository "moi-quota/platform/face/repository/v1"
	face "moi-quota/platform/face/service/v1"
	ocrRepository "moi-quota/platform/ocr/repository/v1"
	ocr "moi-quota/platform/ocr/service/v1"
)

func GetApplicationRegister() []protocol.Service {
	loadBalanceQuotaCollection := mongoDriver.InitCollection(loadBalanceRepository.CollectionName)
	virtualMachineQuotaCollection := mongoDriver.InitCollection(virtualMachineQuotaRepository.CollectionName)
	virtualMachineInstanceCollection := mongoDriver.InitCollection(virtualMachineInstanceRepository.CollectionName)
	firewalldCollection := mongoDriver.InitCollection(firewalldRepository.CollectionName)
	repositoryFirewalld := firewalldRepository.NewMongoRepository(firewalldCollection)
	repositoryLoadBalanceQuota := loadBalanceRepository.NewMongoRepository(loadBalanceQuotaCollection)
	repositoryVirtualMachineInstance := virtualMachineInstanceRepository.NewMongoInstanceRepository(virtualMachineInstanceCollection)
	repositoryVirtualMachineQuota := virtualMachineQuotaRepository.NewRepository(virtualMachineQuotaCollection, repositoryVirtualMachineInstance)
	// platform
	faceQuotaCollection := mongoDriver.InitCollection(faceRepository.CollectionName)
	repositoryFaceQuota := faceRepository.NewMongoRepository(faceQuotaCollection)
	ocrQuotaCollection := mongoDriver.InitCollection(ocrRepository.CollectionName)
	repositoryOcrQuota := ocrRepository.NewMongoRepository(ocrQuotaCollection)
	return []protocol.Service{
		vm.NewProtocolService(context.Background(), repositoryVirtualMachineQuota),
		loadBalance.NewProtocolService(context.Background(), repositoryLoadBalanceQuota),
		firewalld.NewProtocolService(context.Background(), repositoryFirewalld),
		face.NewFaceProtoService(context.Background(), repositoryFaceQuota),
		ocr.NewOCRProtoService(context.Background(), repositoryOcrQuota),
	}
}
