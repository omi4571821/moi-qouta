package v1

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	protocol "moi-quota/pkg/protocol/v1"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var env = os.Getenv("ENV")

type httpServer struct {
	port    string
	tcpPort string
	ctx     context.Context
	opts    []runtime.ServeMuxOption
	apps    []protocol.Service
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		log.Printf("%s %s %s", r.Method, r.RequestURI, time.Since(start))
	})
}
func (h *httpServer) registerAppProtocol(app []protocol.Service) {
	h.apps = append(h.apps, app...)
}

func (h *httpServer) registerHttp(ctx context.Context, conn *grpc.ClientConn) (http.Handler, error) {
	mux := runtime.NewServeMux(h.opts...)
	for _, app := range h.apps {
		err := app.HttpListening(mux, conn)
		if err != nil {
			return nil, err
		}
	}
	return mux, nil
}

func (h *httpServer) listening() error {
	conn, err := grpc.DialContext(h.ctx, ":"+h.tcpPort, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return err
	}
	mux, err := h.registerHttp(h.ctx, conn)
	if err != nil {
		return err
	}
	addr := ":" + h.port
	if env == "prod" {
		addr = "0.0.0.0" + addr
	}

	loggedMux := loggingMiddleware(mux)
	srv := &http.Server{
		Addr:    addr,
		Handler: loggedMux,
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		// sig is a ^C, handle it
		for range c {
			log.Println("shutting down HTTP/REST gateway...")
			_, cancel := context.WithTimeout(h.ctx, 5*time.Second)
			defer cancel()
			_ = srv.Shutdown(h.ctx)
		}

	}()

	log.Println("starting HTTP/REST gateway..." + h.port)
	return srv.ListenAndServe()
}

func (h *httpServer) setPort(ports ...string) {
	if len(ports) == 2 {
		h.port = ports[0]
		h.tcpPort = ports[1]
	}
}

func initHttpServer(opts ...runtime.ServeMuxOption) serverListening {
	return &httpServer{
		ctx:  context.Background(),
		opts: opts,
	}
}
